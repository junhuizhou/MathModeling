%%%%%%%%%%%%%%%%得到等待策略和更改路径策略的路径及时间窗%%%%%%%%%%%%%%%%%%
function [flag,W_TW,W_Pa,C_Pa,C_TW] = Get_WaittingChangePath(DT,map,Pa,TW)
% map  环境地图矩阵
% DT 冲突类型记录矩阵 
global SE;
global Path;
global TimeWindow;
n = length(Path{DT(4)});             %得到AGV1的路径栅格点数            
m = size(DT,1);                      %得到AGV2的可选择路径数
flag = 0;                            %包含类相遇类型标记 0：非包含类
  for i = 1:m
      k = length(Pa);         %得到AGV2选择路径的栅格点数
      if DT(i,1)==2                   %如果为节点冲突
          for j = DT(i,3):k           %更改冲突节点及以后节点的时间窗
              t = Pa(i,j);      %选择当前路径冲突节点及以后节点的栅格
              for z = 1:n
                  if t == Path{DT(i,4)}(z)   %判断是否存在相同栅格点
                      if TW(1,j+1)<TimeWindow{DT(i,4)}(z)||TW(1,j)>TimeWindow{DT(i,4)}(z+1) %时间窗冲突检测
                      else             %若冲突
                        t = abs(TimeWindow{DT(i,4)}(z+1)-TW(i,j));  
                        TW(i,j) = TimeWindow{DT(i,4)}(z+1);
                        TW(i,j+1:k+1) = TW(i,j+1:k+1)+t;
                      end
                  end
              end
          end
      else                                 %如果为相遇冲突
        [x] = find(find(Path{DT(i,4)}==Pa(i,1)));      %判断是否为包含类相遇问题
        if isempty(x)          %（在相遇类型中，若AGV1的路径中包含AGV2路径的
           flag = 0;                  %起点则为包含类相遇，无法采用等待策略）
        for j = DT(i,3):k
            t = Pa(i,j); 
            for z = 1:n
                if t == Path{DT(i,4)}(z)   %判断是否存在相同栅格点
                   if TW(i,j+1)<TimeWindow{DT(i,4)}(z)||TW(i,j)>TimeWindow{DT(i,4)}(z+1) %时间窗冲突检测
                   else
                       if j > n-z       
                          for g = 1:n-z
                              if j-g>0 && Pa(i,j-g)==Path{DT(i,4)}(z+g)
                                 t =TimeWindow{DT(i,4)}(z+g+1)-TW(i,j-g);
                                 TW(i,j-g) = TimeWindow{DT(i,4)}(z+g+1);
                                 TW(i,j-g+1:k+1) = TW(i,j-g+1:k+1)+t;
                              end
                          end
                       else
                           for g = 1:j
                               if j-g>0 && Pa(i,j-g)==Path{DT(i,4)}(z+g)
                                  t = TimeWindow{DT(i,4)}(z+g+1)-TW(i,j-g);
                                  TW(i,j-g) = TimeWindow{DT(i,4)}(z+g+1);
                                  TW(i,j-g+1:k+1) = TW(i,j-g+1:k+1)+t;
                               end
                           end
                       end
                    end
                end
            end
         end
       else
         flag = 1;
        end
      end
  end
  W_TW = TW;
  W_Pa = Pa;
  q = DT(1,2);
  if Pa(1,DT(1,3)) == SE(q,2)          %判断冲突节点是否为AGV的目标节点
      s = Pa(1,DT(1,3)-1);            %若是AGV2的目标节点，将冲突节点更改为前一节点
  else
     s = Pa(1,DT(1,3));                           %得到冲突点位置
  end
     [CTW,CPa] = AlterMap(s,map,W_TW,W_Pa,q);   %临时障碍点处理
     C_TW = CTW;
     C_Pa = CPa;
end
