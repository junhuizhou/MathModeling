%%%%%%%%%%%%%%%%%%%%%%%%%障碍点临时处理%%%%%%%%%%%%%%%%%%%%%%%%%%%
%若所有备选路径都存在冲突，那么将选择的备用路径中的第一条路径作为判断路径，
%将该路径的冲突节点作为该任务中的临时障碍点处理，临时更改境地图map，
%将冲突点作为不可达点处理
function [CTW,CPa]=AlterMap(num,map,TW,Pa,q)
% num  冲突点位置
% map  环境地图
global SE;
PrePathSet = cell(1,1);
PreTimeWindow = cell(1,1);
PrePathIndex = cell(1,1);
    n = size(map);
    if rem(num,n(2))==0            %取余
        col = n(2);
    else
        col = rem(num,n(2));
    end
    if rem(num,n(2))==0 
        row = floor(num/n(2));
    else
        row = floor(num/n(2))+1;    %取整
    end
     map(row,col) = 1;                               %在map中将冲突点设置为临时障碍点
     W=G2D(map);                                     %得到环境地图的邻接矩阵
     W(W==0)=Inf;                                    %邻接矩阵数值处理
     W=OPW(map,W);                                   %优化邻接矩阵     
     W(W==Inf)=999;                                  %邻接矩阵数值处理
     [L,sp,distance]=dijkstraR(W,SE(q,1),SE(q,2),10); %设置始末栅格及路径条数
     if distance<999                                 %若设置临时障碍点能规划出路径
        [X,Y]=Get_xy(distance,sp,map);                   %得到X,Y坐标
        [T,N] = GetTrastion(X,Y,sp);                     %得到所有路径的转弯次数矩阵
        [R] = rank(T);                                   %对转弯矩阵从小到大排序
        [P]=GetPath(L,distance,R);                       %得到约束条数的路径
        [Tw] = Get_TimerWindow(distance,sp,P,N,T);       %得到每条路径时间窗矩阵
        if iscell(sp)==1                                 %数据类型转换
           sp = cell2mat(sp);
        end
        PreTimeWindow{1,1} = Tw;                         %得到预规划路径时间窗                 
        PrePathSet{1,1} = sp;                            %得到预规划路径集合
        PrePathIndex{1,1} = P;                           %得到预规划路径索引
        l = size(PrePathSet{1},1);                     %获取当前路径条数
        for i=1:l
            Index = PrePathIndex{1}(i);
            [Mark] = Detection_TW(PrePathSet{1}(Index,:),PreTimeWindow{1}(Index,:),q);
            if sum(Mark(1,:)) == 0
                CPa = PrePathSet{1}(Index,:);
                CTW = PreTimeWindow{1}(Index,:);
                break;
            end
            if i==l && sum(Mark(1,:)) ~= 0
                CTW = TW;
                CPa = Pa;
            end
        end
     else                             %若设置临时障碍点未规划出路径，采用等待策略
         CTW = TW;
         CPa = Pa;
     end
    
end

