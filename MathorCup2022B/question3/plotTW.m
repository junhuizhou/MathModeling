%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%画出时间窗%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotTW(map,C)
global Path;
global TimeWindow;
n = size(map);     %得到地图尺寸
Y = n(1)*n(2);
b = size(Path,1);   %得到路径条数
y = 0:1:Y;
for i=1:b
    figure(i)
    X = floor(TimeWindow{i}(end)+10);  %得到x轴长度
    x = 0:1:X;
    axis([0 X 0 Y]);              %得到时间窗窗口
    set(gca,'xtick',x,'ytick',y,'GridLineStyle','-',...
        'xGrid','on','yGrid','on');
    hold on
    m = length(Path{i});      %得到每条路径的栅格点数
    for j=1:m
        t = Path{i}(j);       %得到具体的栅格点
        fill([TimeWindow{i}(j),TimeWindow{i}(j),TimeWindow{i}(j+1),TimeWindow{i}(j+1)],[t-1,t,t,t-1],C(i,:));
        hold on
    xlabel(['time /s'],'Fontsize',14);
    ylabel(['栅格号'],'Fontsize',14);
    title(['AGV',num2str(i),' 时间窗'],'Fontsize',18);
    end
end
end

