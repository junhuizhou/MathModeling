%实时脚本ga

%%
function J=GAforQ3(RNG)
rng(RNG);

load('SelectNode.mat');
load('MapInformation.mat');
load('Pallets.mat');

global deltaS;
global S;
global S_bar;
global m_part12;
deltaS=150; %每个蓝点的取货误差限

%计算各个绿点存货量s
[m_part11,n_part11]=size(Pallets);
[m_part12,n_part12]=size(SelectNode);
PalletsTotal=zeros(m_part11,3); %该矩阵存储[X,Y,货量]
for i=1:1:m_part11
    PalletsTotal(i,1:2)=Pallets(i,1:2);
    for j=1:1:Pallets(i,4)
        PalletsTotal(i,3)=Pallets(i,2*j+4)+PalletsTotal(i,3);
    end   
end

TOtal=sum(PalletsTotal(:,3)); %计算总货量
S(:,1)=PalletsTotal(:,3);
% S_bar=ceil(TOtal/m_part12);  %向上取整
S_bar=ceil(TOtal/6);  %向上取整

load('l.mat');
global l;

l_min=zeros(1,m_part11);
for i=1:1:m_part11
    l_min(1,i)=min(l(i,:));
end

nvars=m_part11;
lb=ones(1,m_part11);
ub=18*ones(1,m_part11);
Integer=1:1:m_part11;





% 设置非默认求解器选项
options = optimoptions('ga','PopulationSize',50,'MaxGenerations',10000,...
    'InitialPopulationMatrix',l_min);

% 求解
[solution,objectiveValue] = ga(@fitnessfunction,nvars,[],[],[],[],lb,ub,...
    @ellipsecons2,Integer,options);

% 清除变量
clearvars options

J=fitnessfunction(solution);
[c,ceq] = ellipsecons2(solution);
% disp(J);
end