%%%%%%%%%%%%%得到次数递增的路径顺序向量%%%%%%%%%%%%%%%%%
function [R] = rank( T )
% T 转弯次数矩阵    % R 转弯次数递增路径索引矩阵
n = length(T);
j = 1;
 while min(T)~=999      %遍历标志（遍历后赋值999）
     t = min(T);
     for i = 1:n
         if T(i)==t
             T(i)=999;
             R(j)=i;
             j = j+1;
         end
     end
 end
end
