 %%%%%%%%%%%%%%%%%%%%%%%%时间窗冲突检测%%%%%%%%%%%%%%%%%%%%%%%%
function [Mark] = Detection_TW(Pa,TW,i)
global Path;
global TimeWindow;
mark = 0;                            %冲突标志，0为无冲突
Mark = [];                           %记录冲突路径
k = size(Pa,2);                      %得到当前路径的栅格点数
for j=1:i-1
    for z = 1:k                      %判断当前路径的时间窗是否冲突
         t = Pa(1,z);                %遍历选择路径的经过的每个栅格
         [r,c] = find(Path{j}==t);   %选择路径的当前栅格是否存在于AGV1的路径
         if ~isempty(r)              %如果存在，时间窗冲突检测
             if TW(1,z+1)<TimeWindow{j}(c)||TW(1,z)>TimeWindow{j}(c+1)  %时间窗冲突检测
                  mark = 0;        
             else
                  mark = 1;
                  break;
             end
         end
    end
     Mark(1,j) = mark;
end

