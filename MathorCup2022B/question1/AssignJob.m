%分配车-货架-拣货地点
%输入：货车位置（19*2） 货架位置（19*2） 
%输出： [对应顺序矩阵,总步长]
%顺序矩阵 某行[1 3 X Y] 1代表输入的1号车 3代表输入的3号货架 X Y为拣货地点的坐标
function [MatrixOrder,TotalStep]=AssignJob(CarPoint,StoragePoint)

global MapData;
[m1,n1]=size(CarPoint);
[m2,n2]=size(StoragePoint);
MatrixOrder=zeros(m1,4);
%建立车与货架的步数关系
for i=1:1:m1
   for j=1:1:m2
       MobileOrder=GetTrajectory([CarPoint(i,:);StoragePoint(j,:)]);
       [M,N]=size(MobileOrder);
       DisCar2G(i,j)=M-1;
   end
end

[matching,step1] = Hungarian(DisCar2G);%匈牙利算法 回溯求得取点与总步数
%将取点赋值给输出
for i=1:1:m1
   for j=1:1:m2
       if matching(i,j)==1
           MatrixOrder(i,1)=i;
           MatrixOrder(i,2)=j;
           break;
       end
   end
end


[m3,n3]=size(MapData);
%找到拣选节点
flag=1;
for i=1:1:m3
   if  MapData(i,1)==5
       SelectPoint(flag,:)=MapData(i,2:3);
       flag=flag+1;
   end
end
[m4,n4]=size(SelectPoint);

%建立绿点与蓝点间的步数关系
for i=1:1:m2
   for j=1:1:m4
       MobileOrder2=GetTrajectory([StoragePoint(i,:);SelectPoint(j,:)]);
       [M2,N2]=size(MobileOrder2);
       G2B(i,j)=M2-1;
   end
end
matching2=min(G2B,[],2);%找到每行的最小值
%找到每个绿点对应的蓝点坐标
for i=1:1:m2
    for j=1:1:m4
        if matching2(i,1)==G2B(i,j)
            MatrixOrder2(i,:)=SelectPoint(j,:);
            break;
        end
    end
end
%根据小车对应绿点再对应蓝点的关系完成输出矩阵
for i=1:1:m1
    if MatrixOrder(i,2)>0
        MatrixOrder(i,3:4)=MatrixOrder2(MatrixOrder(i,2),:);
    end
end

step2=sum(matching2);%计算蓝绿最小步长和

TotalStep=step1+step2;
end