% 快排：所有行根据第keynum列的数值从大到小排序
function input = QuickSort(input, leftindex, rightindex, keynum)
    if leftindex < rightindex
        i = leftindex;
        j = rightindex;
        temp = input(i, :);
        while i < j
            while (i<j)&&(input(j, keynum) <= temp(keynum))
                j = j-1;
            end
            input(i, :) = input(j, :);
            while (i<j)&&(input(i, keynum) >= temp(keynum))
                i = i+1;
            end
            input(j, :) = input(i, :);
        end
        
        input(j, :) = temp;
        input = QuickSort(input, leftindex, j-1, keynum);
        input = QuickSort(input, i+1, rightindex, keynum);
    end
end