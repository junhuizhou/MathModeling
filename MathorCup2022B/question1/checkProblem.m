clear all;
clc;
Batch = csvread('Batch.csv');

bag = zeros(1);
h = 0;
for i = 1:1:size(Batch, 1)
    
    for j = 1:1:size(Batch, 2)
        if size(find(bag(1,:)==Batch(i,j)), 2) == 0
            h = h+1;
            bag(h) = Batch(i,j);
        else
            [i,j]
            Batch(i,j)
        end
    end
end

Batch(3,20) = 10144;
Batch(7,20) = 10146;
save('Batch.mat','Batch');

pack = 10000:1:10147;
for i = 1:1:size(Batch, 1)
    for j = 1:1:size(Batch, 2)
        if size(find(pack(1,:)==Batch(i,j)), 2) ~= 0
            [x,y] = find(pack(1,:)==Batch(i,j));
            pack(:,y) = [];
        end
    end
end