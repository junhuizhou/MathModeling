%找到最近的红点
%输入：车坐标(1*2)
%输出：到最近的红点[步数，路径(n*2)]

function [STEP,PATH]=FindNearestRed(input)


MobileOrder1=GetTrajectory([input(1,:);32,21]);
[m1,n1]=size(MobileOrder1);
step1=m1-1;

MobileOrder2=GetTrajectory([input(1,:);2,1]);
[m2,n2]=size(MobileOrder2);
step2=m2-1;

if step1<step2
    STEP=step1;
    PATH=MobileOrder1;
else
    STEP=step2;
    PATH=MobileOrder2;   
end


end