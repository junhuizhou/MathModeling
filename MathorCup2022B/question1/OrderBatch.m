clear all;
clc;
load('OrderData.mat');
load('PalletsData.mat');

% 按商品需求量对订单排序
SKUMaxOrder = OrderData;
SKUMaxOrder = QuickSort(SKUMaxOrder, 1, size(SKUMaxOrder,1), 2);
m = size(SKUMaxOrder, 1);

% 计算maxPalletCount方便定义SKUPalletNum
maxPalletCount = 1;
for i = 1:1:m
    SKUPallet = FindSKU(SKUMaxOrder(i,1));
    row = size(SKUPallet, 1);
    if row > maxPalletCount
        maxPalletCount = row;
    end
end

% SKUPalletNum = [商品类SKU,货架1,货架1该SKU的数量,货架2,...]
SKUPalletNum = zeros(m, 2*maxPalletCount + 1);
for i = 1:1:m
    SKUPallet = FindSKU(SKUMaxOrder(i,1));
    row = size(SKUPallet, 1);
    % 排序
    if row > 1
        SKUPallet = QuickSort(SKUPallet, 1, row, 4);
    end
    SKUPalletNum(i, 1) = SKUMaxOrder(i,1);
    % 存数据
    for j = 1:1:row
        SKUPalletNum(i, 2*j:2*j+1) = SKUPallet(j, 3:4);
    end
end

% 更新数据表
tmpPallets = PalletsData;
tmpSKUMaxOrder = SKUMaxOrder;
tmpSKUPalletNum = SKUPalletNum;

% 初始空分批
Batch = zeros(1, 19);
brow = 0;

% 大循环
flag = 0;
while 1
    brow = brow + 1;
    bcol = 0;
    lastbcol = 0;
    flag = flag + 1;
    
    deleteFlag = 0;
    while 1
        % 清理下0数据
        i = 1;
        while i <= size(tmpSKUMaxOrder,1)
            if tmpSKUMaxOrder(i,2)==0
                tmpSKUMaxOrder(i,:) = [];
                tmpSKUPalletNum(i,:) = [];
                i = i-1;
            end
            i = i + 1;
        end
        % 先取前19订单计算分数并排序取前19名货架（假设订单数<=需求货架数）
        kSKUOrderNum = 19;
        kSKUPalletNum = 19;
        if kSKUOrderNum > size(tmpSKUMaxOrder,1)
            kSKUOrderNum = size(tmpSKUMaxOrder,1);
        end
        if kSKUPalletNum > size(tmpSKUPalletNum, 1)
            kSKUPalletNum = size(tmpSKUPalletNum, 1);
        end
        
        palletGrade = zeros(1,2);
        h = 1;
        cnt = 0;
        for i = 1:1:kSKUOrderNum
            for j = 2:2:2*maxPalletCount
                % 判断是否有货架
                if tmpSKUPalletNum(i,j)==0
                    break;
                end
                % 计算当前启发式分数
                tmpGrade = tmpSKUMaxOrder(i,2);
                tmpGrade = tmpGrade + ZerotoOne(tmpSKUPalletNum(i,j+1)/tmpGrade);
                % 判断货架是否已经在Batch里面
                if bcol ~= 0
                    if size(find(Batch(brow,:)==tmpSKUPalletNum(i,j)), 2) ~= 0
                        continue;
                    end
                end
                % 判断货架是否已经在分数内
                if size(find(palletGrade(:,1)==tmpSKUPalletNum(i,j)), 1) ~= 0
                    [row,col] = find(palletGrade(:,1)==tmpSKUPalletNum(i,j));
                    palletGrade(row,2) = palletGrade(row,2) + tmpGrade;
                else
                    h = h+1;
                    palletGrade(h,1) = tmpSKUPalletNum(i,j);
                    palletGrade(h,2) = tmpGrade;
                end
            end
        end
        palletGrade = QuickSort(palletGrade, 1, h, 2);
        % 只取满足首个订单的货架
        partSKUPalletNum = tmpSKUPalletNum(1,2:end);
        partSKUPalletNum(find(partSKUPalletNum==0)) = [];
        partpalletGrade = zeros(size(partSKUPalletNum,2)/2, 3);
        for i = 1:2:size(partSKUPalletNum,2)-1
            if size(find(palletGrade(:,1)==partSKUPalletNum(i)), 1) ~= 0
                [row, col] = find(palletGrade(:,1)==partSKUPalletNum(i));
                partpalletGrade((i+1)/2, 1) = partSKUPalletNum(i);
                partpalletGrade((i+1)/2, 2) = partSKUPalletNum(i+1);
                partpalletGrade((i+1)/2, 3) = palletGrade(row,2);
            end
        end
        partpalletGrade = QuickSort(partpalletGrade, 1, size(partpalletGrade,1), 3);
        % 更新首订单货架中所取部分
        deleteFlag = 0;
        tmpNeed = tmpSKUMaxOrder(1,2);
        for i = 1:1:size(partpalletGrade,1)
            if tmpNeed > 0
                tmpNeed = tmpNeed - partpalletGrade(i, 2);
                bcol = bcol + 1;
                Batch(brow, bcol) = partpalletGrade(i, 1);
            end
            if bcol == 19
                break;
            end
        end
        if tmpNeed <= 0
            deleteFlag = 1;
        end
        % 删除或者更改首订单数据
        if deleteFlag == 1
            tmpSKUMaxOrder(1,:) = [];
            tmpSKUPalletNum(1,:) = [];
        else
            tmpSKUMaxOrder(1,2) = tmpNeed;
            %数值赋值0再来一次排序
            tmpCount = i;
            tmpMat = tmpSKUPalletNum(1,:);
            for j = 1:1:tmpCount
                k = find(tmpMat(1,:)==partpalletGrade(j, 1));
                tmpMat(:, k:k+1) = [];
                tmpMat(1, 28:29) = [0,0];
            end
            tmpSKUPalletNum(1,:) = tmpMat;
        end
        
        % 统计一下Batch的效果
        tmpBatchRecord = zeros(1,2);
        h = 1;
        for i = lastbcol+1:1:bcol
            row = Batch(brow, i) - 9999;
            for j = 5:2:5+2*(tmpPallets(row,4)-1)
                if size(find(tmpBatchRecord(:,1)==tmpPallets(row,j)), 1) ~= 0
                    [x,y] = find(tmpBatchRecord(:,1)==tmpPallets(row,j));
                    tmpBatchRecord(x,2) = tmpBatchRecord(x,2) + tmpPallets(row,j+1);
                else
                    h = h + 1;
                    tmpBatchRecord(h,1:2) = tmpPallets(row,j:j+1);
                end
            end
        end
        tmpBatchRecord = QuickSort(tmpBatchRecord, 1, h, 2);
        % 根据Batch的效果更新数据tmpSKUMaxOrder
        restRecord = zeros(1,2);
        rR = 0;
        beginrow = 0;
        if deleteFlag == 1
            beginrow = 1;
        else
            [row, col] = find(tmpBatchRecord(:,1)==tmpSKUMaxOrder(1,1));
            beginrow = row + 1;
        end
        for i = beginrow:1:size(tmpBatchRecord,1)-1
            if size(find(tmpSKUMaxOrder(:,1)==tmpBatchRecord(i,1)),1) ~= 0
                [row, col] = find(tmpSKUMaxOrder(:,1)==tmpBatchRecord(i,1));
                if tmpSKUMaxOrder(row, 2) >= tmpBatchRecord(i,2)
                    tmpSKUMaxOrder(row, 2) = tmpSKUMaxOrder(row, 2) - tmpBatchRecord(i,2);
                else
                    tmpSKUMaxOrder(row, 2) = 0;
                    %记录下哪种商品多出，倒推出托盘号
                    [x,y] = find(tmpSKUPalletNum(:,1)==tmpBatchRecord(i,1));
                    for j = 1:1:19
                        if size(find(tmpSKUPalletNum(x,:)==Batch(brow, j)), 2) ~= 0
                            [u,v] = find(tmpSKUPalletNum(x,:)==Batch(brow, j));
                            if size(find(restRecord(:,1)==Batch(brow, j)),1) == 0
                                rR = rR + 1;
                                restRecord(rR,:) = tmpSKUPalletNum(x,v:v+1);
                            end
                        end
                    end
                end
            end
        end
        
        % 更新tmpSKUPalletNum数据
        % 无剩余情况
        beginrow = 0;
        if deleteFlag == 1
            beginrow = 1;
        else
            beginrow = 2;
        end
        tmpMat = zeros(1, 2*maxPalletCount + 1);
        for i = 1:1:(size(tmpSKUPalletNum,1))
            for j = lastbcol+1:1:bcol
                if size(find(tmpSKUPalletNum(i,:)==Batch(brow,j)), 2) ~= 0
                    [row, col] = find(tmpSKUPalletNum(i,:)==Batch(brow,j));
                    tmpMat = tmpSKUPalletNum(i, :);
                    tmpMat(:, col:col+1) = [];
                    tmpMat(1, 28:29) = [0,0];
                    tmpSKUPalletNum(i, :) = tmpMat;
                end
            end
        end
        
        lastbcol = bcol;
        % 跳出条件
        if bcol == 19
            break;
        end
    end
    
    if flag == 8
        break;
    end
    %     if size(tmpSKUMaxOrder,1) == 0
    %         break;
    %     end
    
end

% Batch(3,20) = 10144;
% Batch(7,20) = 10146;
% save('Batch.mat','Batch');
% writematrix(Batch, 'Batch.csv');

function y = ZerotoOne(x)
if x > 1
    y = 1;
else
    y = x;
end
end

