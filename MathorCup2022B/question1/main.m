%主函数

clc;
clear all;

global MapData;
global Pallets2;
global AgvData;
global Batch;
load('MapData.mat');
load('PalletsData.mat');
load('AgvData.mat');
load('Batch.mat');

AgvData(11,:)=[]; %除去问题小车
Pallets2(:,1:3)=PalletsData(:,1:3);
Pallets2(:,4)=ones(148,1); %[绿点X,绿点Y，货架号，是否有货]；

batch=ones(8,38);
for i=1:1:8
    for j=1:1:19
        batch(i,2*j-1)=Batch(i,j);
    end
end
batch(3,34)=0;
batch(7,32)=0;
batch(8,17:38)=zeros(1,22);

for i=1:1:8
    for j=1:1:19
        for p=i:1:8
            if p==i
                q1=j+1;
            elseif p==i&&j==19
                p=p+1;
                q=1;
            else
                q1=1;
            end
            for q=q1:1:19
                if Batch(i,j)==Batch(p,q)
                    batch(i,2*j)=0;
                    break;
                end
            end
        end
    end
end

[STEP,PATH]=CompeletAssign(batch);
% save('PATH1.mat','PATH');
