%找到最近的绿点
%输入：车坐标(1*2),空闲绿点坐标
%输出：到最近的绿点[步数，路径(n*2)]

function [STEP,PATH,X,Y]=FindNearestGreen(inputCar,inputGreen)

[m,n]=size(inputGreen);
STEP=10000;
FLAG=1;
X=0;
Y=0;
for i=1:1:m
    MobileOrder=GetTrajectory([inputCar(1,:);inputGreen(i,:)]);
    [m1,n1]=size(MobileOrder);
    step=m1-1;
    if step<STEP
        STEP=step;
        FLAG=i;
        X=inputGreen(i,1);
        Y=inputGreen(i,2);
    end
end

PATH=GetTrajectory([inputCar(1,:);inputGreen(FLAG,:)]);



end