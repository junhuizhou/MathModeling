%分配车-货架-拣货地点
%输入：货车位置（19*2） 货架位置（19*2） 
%输出： [对应顺序矩阵,总步长]
%顺序矩阵 某行[1 3 X Y] 1代表输入的1号车 3代表输入的3号货架 X Y为拣货地点的坐标
function [MatrixOrder,TotalStep]=AssignJob(CarPoint,StoragePoint)

global MapData;
global solution;
global Pallets2;
global SelectNode;

[m1,n1]=size(CarPoint);
[m2,n2]=size(StoragePoint);
MatrixOrder=zeros(m1,4);
%建立车与货架的步数关系
for i=1:1:m1
   for j=1:1:m2
       MobileOrder=GetTrajectory([CarPoint(i,:);StoragePoint(j,:)]);
       [M,N]=size(MobileOrder);
       DisCar2G(i,j)=M-1;
   end
end

[matching,step1] = Hungarian(DisCar2G);%匈牙利算法 回溯求得取点与总步数
%将取点赋值给输出
for i=1:1:m1
   for j=1:1:m2
       if matching(i,j)==1
           MatrixOrder(i,1)=i;
           MatrixOrder(i,2)=j;
           break;
       end
   end
end


[m3,n3]=size(MapData);
%找到拣选节点
 
 OrderInPallets2=zeros(m1,1);
for i=1:1:m1 
    for j=1:1:148  
        if MatrixOrder(i,2)>0
            if StoragePoint(MatrixOrder(i,2),1)==Pallets2(j,1) && StoragePoint(MatrixOrder(i,2),2)==Pallets2(j,2)
                OrderInPallets2(i,1)=j;
                break;
            end
        end
    end
end

 OrderInSolution=zeros(m1,1);
 for i=1:1:m1
     if MatrixOrder(i,2)>0
         OrderInSolution(i,1)=solution(1,OrderInPallets2(i,1));
     end
 end

 MatrixOrder2=zeros(m1,2);
 for i=1:1:m1
     if MatrixOrder(i,2)>0
         MatrixOrder2(i,1:2)=SelectNode(OrderInSolution(i,1),2:3);
     end
 end
 
matching2=zeros(m1,1);
for i=1:1:m1
    if MatrixOrder(i,2)>0
       MobileOrdermatching2=GetTrajectory([StoragePoint(MatrixOrder(i,2),1:2);MatrixOrder2(i,1:2)]);
        [mMobile2,nMobile2]=size(MobileOrdermatching2);
        matching2(i,1)=mMobile2-1;
    end
end


%根据小车对应绿点再对应蓝点的关系完成输出矩阵
for i=1:1:m1
    if MatrixOrder(i,2)>0
        MatrixOrder(i,3:4)=MatrixOrder2(i,1:2);
    end
end

step2=sum(matching2);%计算蓝绿最小步长和

TotalStep=step1+step2;
end