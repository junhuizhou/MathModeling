%找到对应点的行数
%输入 点坐标（1*2）
%输出 行数
function LineNum=FindLineNum(input)
% load('MapData.mat');
global MapData;
[m,n]=size(MapData);
for i=1:1:m
    if input(1,1)==MapData(i,2)&&input(1,2)==MapData(i,3)
        LineNum=i;
        break;
    end
end

end