%遗传算法约束函数 18点
function [c,ceq] = ellipsecons(K)
global S;
global S_bar;
global deltaS;
global m_part12;
K=K';
[m,n]=size(K);
K1=zeros(m,n);
for i=1:1:m
    K1(i,1)=ceil(K(i,1));    
end
k=zeros(m,m_part12);
for i=1:1:m
    k(i,K1(i,1))=1;    
end
B=sum(k.*S);

S_UP=S_bar+deltaS;
S_LOW=S_bar-deltaS;

c=zeros(2*m_part12,1);

for i=1:1:m_part12
    c(i,1)=B(1,i)-S_UP;
    c(i+18,1)=S_LOW-B(1,i);
end
c=c';
ceq=[];

end