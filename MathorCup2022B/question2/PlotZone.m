%第二题第一问画图
clc;
clear all;
load('MapData.mat');
load('SelectNode.mat');
load('PalletsData.mat');
load('solution150ini675010000.mat');

cmap=[0.75 0.75 0.75;  % 1 灰色 地面
         0    1    0; % 2 绿色 
         1    1    0;% 3 黄色 
         0    0    0;% 4 黑色 
         0    0    1;% 5 蓝色 
         1    0    1;% 6 粉色 
         1    0    0;% 7 红色 
    ];
colormap(cmap);
Color=ones(23,33);


for i=1:1:18
    aa=ceil(i/3)+1;
    Color(SelectNode(i,3),SelectNode(i,2))=aa;
end

for i=1:1:148
    aa=ceil(solution(1,i)/3)+1;
    Color(PalletsData(i,2),PalletsData(i,1))=aa;
end

pcolor(Color);

% J=fitnessfunction(solution);
% [c,ceq] = ellipsecons(solution);
