Dir question2

1.Dir dataProcessing ==> Dir question2 :
    {
        mapclear.csv;
        AgvData.mat;
        MapData.mat;
        OrderData.mat;
        PalletsData.mat;
    }
2.Dir question1 ==> Batch.mat
3.CalculDistance.m ==> {G2BDistance.mat;StorageNode.mat;SelectNode.mat}
4.GAforQ2 ==> solution.mat :
    {
        <-SelectNode.mat;
    }
5.PlotZone ==> DevideZone.fig :
    {
        <-solution.mat;
        <-SelectNode.mat
    }
6.main.m ==> PATH2.mat :
    {
        <-solution.mat
        <-SelectNode.mat
        <-Batch.mat
    }
7.PlotQuestion2 ==> 1-19.fig :
    {
        <-PATH1.mat
        <-mapclear.csv
    }