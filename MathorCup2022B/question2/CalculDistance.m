%计算绿点（2）与蓝点（5）间距离
%数据用于计算旅行商问题
%共三个文件
%G2BDistance 绿点至蓝点距离
%StorageNode 绿点顺序
%SelectNode  蓝点顺序
clc;
clear;
global MapData;
load('MapData.mat');
[m,n]=size(MapData);
FlagStorageNode=1;
for i=1:1:m
    if  MapData(i,1)==2
        StorageNode(FlagStorageNode,1)=i;
        StorageNode(FlagStorageNode,2:3)=MapData(i,2:3);
        FlagStorageNode=FlagStorageNode+1;
    end
end

FlagSelectNode=1;
for i=1:1:m
    if  MapData(i,1)==5
        SelectNode(FlagSelectNode,1)=i;
        SelectNode(FlagSelectNode,2:3)=MapData(i,2:3);
        FlagSelectNode=FlagSelectNode+1;
    end
end

[m1,n1]=size(StorageNode);
[m2,n2]=size(SelectNode);

for i=1:1:m1
    for j=1:1:m2
        Trajectory=GetTrajectory([StorageNode(i,2:3);SelectNode(j,2:3)]);
        [m3,n3]=size(Trajectory);
        G2BDistance(i,j)=m3-1;
    end
end

save('G2BDistance.mat','G2BDistance');
save('StorageNode.mat','StorageNode');
save('SelectNode.mat','SelectNode');