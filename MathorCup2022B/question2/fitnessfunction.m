%遗传算法适应性函数
function J = fitnessfunction(K)
global l;
global m_part12;
K=K';
[m,n]=size(K);
for i=1:1:m
    K(i,1)=ceil(K(i,1));
end
k=zeros(m,m_part12);
for i=1:1:m
    k(i,K(i,1))=1;
end
J=sum(sum(k.*l));
if J<100
    keyboard;
end

end