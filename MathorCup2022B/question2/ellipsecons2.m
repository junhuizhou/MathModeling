%遗传算法约束函数 6点
function [c,ceq] = ellipsecons2(K)
global S;
global S_bar;
global deltaS;
global m_part12;
K=K';
[m,n]=size(K);
K1=zeros(m,n);
for i=1:1:m
    K1(i,1)=ceil(K(i,1));    
end
k=zeros(m,m_part12);
for i=1:1:m
    k(i,K1(i,1))=1;    
end
B1=sum(k.*S);
B=zeros(1,6);
for i=1:1:6
   B(1,i)= B1( 1,(3*i-2) )+B1( 1,(3*i-1) )+B1( 1,(3*i) );
end

S_UP=S_bar+deltaS;
S_LOW=S_bar-deltaS;

c=zeros(2*6,1);

for i=1:1:6
    c(i,1)=B(1,i)-S_UP;
    c(i+6,1)=S_LOW-B(1,i);
end
c=c';
ceq=[];

end