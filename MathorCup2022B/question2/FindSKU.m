%根据SKU找到货架号及货架位置
%输入SKU值
%输出SKU对应的货架号XY数量（n*4）
function output=FindSKU(input)

load('PalletsData.mat');
[m,n]=size(PalletsData);
flag=1;

for i=1:1:m
    
    for j=1:1:PalletsData(i,4)
        
        if input==PalletsData(i,3+2*j)
            output(flag,1:3)=PalletsData(i,1:3);
            output(flag,4)=PalletsData(i,4+2*j);
            flag=flag+1;
            break;
        end
        
    end
    
    
end


end