%主函数

clc;
clear all;

global SelectNode;
global MapData;
global Pallets2;
global AgvData;
global Batch;
global PATHFor3;
global solution;

load('SelectNode.mat');
load('MapData.mat');
load('PalletsData.mat');
load('AgvData.mat');
load('Batch.mat');
load('solution150ini675010000.mat');


AgvData(11,:)=[]; %除去问题小车
Pallets2(:,1:3)=PalletsData(:,1:3);
Pallets2(:,4)=ones(148,1); %[绿点X,绿点Y，货架号，是否有货]；

% kk=10000;
% for i=1:1:8
%     for j=1:1:19
%         if kk<10148
%             Batch(i,j)=kk;
%             kk=kk+1;
%         else
%             Batch(i,j)=kk-i-j;
%         end
%     end
% end
% Batch(9,1)=10122;
% Batch(9,2:19)=zeros(1,18);
%处理分批数据
batch=ones(8,38);
for i=1:1:8
    for j=1:1:19
        batch(i,2*j-1)=Batch(i,j);
    end
end
batch(3,34)=0;
batch(7,32)=0;
batch(8,17:38)=zeros(1,22);

for i=1:1:8
    for j=1:1:19
        for p=i:1:8
            if p==i
                q1=j+1;
            elseif p==i&&j==19
                p=p+1;
                q=1;
            else
                q1=1;
            end
            for q=q1:1:19
                if Batch(i,j)==Batch(p,q)
                    batch(i,2*j)=0;
                    break;
                end
            end
        end
    end
end

[STEP,PATH]=CompeletAssign(batch);
