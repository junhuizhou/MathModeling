%商品统计
clc;
clear;

filename = 'ordersclear.csv' ;
OrdersClear=csvread(filename);   %读

[m,n]=size(OrdersClear);

OrderData=zeros(100,2);
flag=1;
count=0;
for i=1:1:m
    if OrdersClear(i,1)~=0
        OrderData(flag,1)=OrdersClear(i,2);
        count=OrdersClear(i,3);
        
        for j=i+1:1:m
            if OrdersClear(i,2)==OrdersClear(j,2)
                count=count+OrdersClear(j,3);
                OrdersClear(j,1)=0;
            end
        end
        OrderData(flag,2)=count;
        count=0;
        flag=flag+1;
    end
end

save('OrderData.mat','OrderData');