%读取货架信息
clc;
clear;

filename = 'palletsclear1.csv' ;
Pallets1=csvread(filename);   %  X Y 货架编号
for i=1:1:148
    for j=1:1:2
        Pallets1(i,j)=Pallets1(i,j)+1;
    end
end

fid = fopen('palletsclear2.csv');   %打开并获取csv文件
% [A,COUNT]=fscanf(fid,'%s',inf);
Pallets2=zeros(148,10);  %行数为第x个货架 货号1 数量1 货号2 数量2 货号3 数量3......

for i=1:1:148
    A1=fgetl(fid);
    num_str = regexp(A1,'\d*\.?\d*','match');
    [M,N]=size(num_str);
    for j=1:1:N
        Pallets2(i,j)=str2double(num_str(1,j));
    end
end
fclose(fid);
PalletsData=zeros(148,14);
for i=1:1:148
    count=0;
    for j=1:1:10
        if Pallets2(i,j)~=0
            count=count+1;
        end
    end
    PalletsData(i,4)=count/2;
end
PalletsData(:,1:3) = Pallets1;
PalletsData(:,5:14) = Pallets2;
save('PalletsData.mat','PalletsData');

% Pallets2=zeros(COUNT,10);  %行数为第x个货架 货号1 数量1 货号2 数量2 货号3 数量3......
% flag=1;
% i=1;
% B=zeros(1000,1000);
% while i<3142
%
%     if strcmp(A(1,i) ,'"')
%         flag1=i;
%     end
%     i=i+1;
%     while strcmp(A(1,i) ,'"')~=1
%          i=i+1;
%     end
%     flag2=i;
%
%     B(flag,flag1+1:flag2-1)=A(1,flag1+1:flag2-1);
%     num_str = regexp(B(flag,flag1+1:flag2-1),'\d*\.?\d*','match');
%     [M,N]=size(num_str);
%     for j=1:1:N
%         Pallets2(flag,j)=str2double(num_str(1,j));
%     end
%
%     flag=flag+1;
%     i=i+1;
%
% end

