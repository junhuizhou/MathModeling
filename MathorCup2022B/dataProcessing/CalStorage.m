%计算仓库总存货量、订单总需求量、库存剩余量
clc;
clear;

load('PalletsData.mat');
[m,n]=size(PalletsData);

load('OrderData.mat');
[M,N]=size(OrderData);

for i=1:1:M
    count=0;
   for j=1:1:m
       for k=1:1:PalletsData(j,4)
           if PalletsData(j,3+2*k)==OrderData(i,1)
              count=count+ PalletsData(j,4+2*k);
              break;
           end           
       end
   end   
    StorageTotal(i,1)=OrderData(i,1);
    StorageTotal(i,2)=count;
end


StorageRemain=StorageTotal(:,2)-OrderData(:,2);
save('StorageTotal.mat','StorageTotal');
save('StorageRemain.mat','StorageRemain');
