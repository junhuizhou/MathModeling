% 进化策略算法求解二次函数y = 21.5+x1*sin(4*pi*x1)+x2*sin(20*pi*x2)的最大值
% 变量定义域：x1∈[-3.0, 12.1], x2∈[4.1, 5.8]
% 参考值x1=11.1737, x2=5.8367, y=38.8363
% ES个图强调受控的自然变异方式；GA偏重于随机的变化
% ES个体由两部分实数编码值构成，一为实数基因值表示解，二为解对应位置的编译强度；GA基因值改变则是通过概率
% ES新种群的选择多采用精英策略；GA多采用轮盘赌或锦标赛策略

clear all;
close all;
clc;

% 随机生成的初始种群由μ=miu个个体组成
miu = 40;
x1 = 15.1 * rand(1, miu) - 3; % 生成初始解
x2 = 1.7 * rand(1, miu) + 4.1;
X = [x1; x2]; % 初始解X矩阵，一列表示一个可行解
sigma = rand(2, miu); % 标准差向量σ=sigma
MaxIter = 200; % 迭代次数
maxy = 0; % 记录最大适应度

for Iter = 1:MaxIter
    lamda = 1;

    % 产生lamda个新个体，lamda>miu，此处选择lamda=7*miu
    while lamda <= 7 * miu
        pos = 1 + fix(rand(1, 2) * (miu - 1)); % 随机产生1~miu之间的整数值，指定两个位置
        pa1 = X(:, pos(1)); % 提取两个位置的(X,σ)做离散重组
        pa2 = X(:, pos(2));
        % X采用离散重组
        if rand() < 0.5 % 随机选出x1
            option(1) = pa1(1);
        else
            option(1) = pa2(1);
        end

        if rand() < 0.5 % 随机选出x2
            option(2) = pa1(2);
        else
            option(2) = pa2(2);
        end

        % sigma采用中值重组
        sigmal = 0.5 * (sigma(:, pos(1)) + sigma(:, pos(2)));
        Y = option' + sigmal .* randn(2, 1); % 计算更新值，t—t'=1,randnO产生正态分布

        if Y(1) >= -3 && Y(1) <= 12.1 && Y(2) >= 4.1 && Y(2) <= 5.8 % 判断Y界
            offspring(:, lamda) = Y; % 未超界，保存子代
            lamda = lamda + 1;
        end

    end % end of while

    U = [offspring]; %采用u,lamda策略，得到lamda个新解

    % 计算目标解
    for i = 1:size(U, 2)
        temp = U(:, i);
        x1 = temp(1);
        x2 = temp(2);
        eva(i) = f2(x1, x2);
    end

    % 从lamda个后代，选出miu个最好解
    [m_eval, I] = sort(eva); % 从小到大排序适应度
    I1 = I(end - miu + 1:end); % 从7*miu子代中选出最好的miu个适应度下标行向量
    X = U(:, I1); % 得到7*miu中最好的miu个个体
    % 更新最大目标值max y，同时记录x1, x2值
    if m_eval(end) > maxy
        maxy = m_eval(end);
        opmx = U(:, end);
    end

    max_y(Iter) = maxy; % 最大值
    mean_y(Iter) = mean(eva(I1)); % 平均值
end % end of Iter

plot(1:MaxIter, max_y, 'b', 1:MaxIter, mean_y, 'g'); % 画图
legend('最大值', '平均值');
opmx % 显示最好解
maxy % 二元函数求最大值函数

function y = f2(x1, x2)
    y = 21.5 + x1 * sin(4 * pi * x1) + x2 * sin(20 * pi * x2);
end
