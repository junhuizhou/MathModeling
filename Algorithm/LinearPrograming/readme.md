<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-17 11:38:30
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-17 17:04:10
 * @FilePath: \MathModeling\Algorithm\LinearPrograming\readme.md
 * @Description: file content
-->

# 线性规划Matlab实现

## Matlab函数

* linprog()
* optimproblem()

## 文件树
