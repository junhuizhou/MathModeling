% 使用linprog()解线性规划问题
%例  max   z=10x1+5x2
%    s.t. 5x1+2x2<=8
%         3x1+4x2=9
%         x1+x2>=1
%         x1,x2>=0

clear all;
clc;
%参数设置
C = [-10; -5];      %目标函数min
A = [5 2; -1 -1];   %不等式约束左边
b = [8; -1];        %不等式约束右边
Aeq = [3 4];        %等式约束左边
beq = 9;            %等式约束右边
lb = zeros(2,1);    %下界
%求解
[x, fval] = linprog(C, A, b, Aeq, beq, lb);
xmax = x
zmax = -fval
