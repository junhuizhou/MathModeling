% 使用optimproblem()求解线性规划问题
%例  max   z=10x1+5x2
%    s.t. 5x1+2x2<=8
%         3x1+4x2=9
%         x1+x2>=1
%         x1,x2>=0

clear all;
clc;
%参数设置
C = [-10; -5];      %目标函数min
A = [5 2; -1 -1];   %不等式约束左边
b = [8; -1];        %不等式约束右边
Aeq = [3 4];        %等式约束左边
beq = 9;            %等式约束右边
lb = zeros(2,1);    %下界
%创建线性规划问题
prob = optimproblem('ObjectiveSense', 'min');
%创建决策变量
X = optimvar('x', 2, 1, 'LowerBound', 0);
prob.Objective = C' * X;
%创建线性约束
cons1 = A * X <= b;
cons2 = Aeq * X == beq;
prob.Constraints.cons1 = cons1;
prob.Constraints.cons2 = cons2;
%求解
[sol, fval, flag, out] = solve(prob);
xmax = sol.x
zmax = -fval
