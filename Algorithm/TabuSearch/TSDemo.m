% 使用禁忌搜索算法求解TSP问题

clear all;
close all;
clc;

city = [8.54, 0.77, 17.02, 0.55, 18.47, 0.61, 10.36, 8.39, 4.85, 17.08, ...
            3.38, 9.59, 7.01, 16.62, 10.84, 2.58, 5.02, 5.78, 17.33, 7.43;
        4.15, 2.52, 4.41, 12.03, 0.70, 11.51, 16.24, 4.47, 1.63, 13.80, ...
            11.28, 4.66, 8.82, 12.65, 5.22, 9.67, 16.23, 6.34, 6.51, 0.55]; % 城市坐标
city = city';

% 参数设置
cityNum = size(city, 1); % 城市数量
TLLength = ceil(cityNum ^ 0.5); % 禁忌表长度
candidateNum = 2 * cityNum; % 邻域解数量，不大于n*(n-1)/2的整数n=cityNum
maxTimes = 1000; % 最大迭代次数

distanceMatrix = getDistanceMatrix(cityNum, city); % 计算并返回距离矩阵
TL = zeros(cityNum); % 初始化禁忌表
beCandsNum = 6; % 邻域解集数量
bestFitnessValue = Inf; % 初始化距离值
initSolution = randperm(cityNum); % 随机生成一个初始解
beCands = ones(beCandsNum, 4); % 邻域解信息四元组：邻域解标号、邻域解距离和邻域交换的两个城市编号
bestSolution = initSolution; % 记录最优解
currentSolution = initSolution; % 记录当前解
CandsList = zeros(candidateNum, cityNum); % 记录所有邻域解
currentTime = 1; % 记录迭代次数
F = zeros(1, candidateNum); % 保存候选解

while (currentTime < maxTimes)
    A = Neiborhood(cityNum, candidateNum); % 返回一组不重复的邻域交换位置

    for i = 1:candidateNum % 生成所有邻域解
        CandsList(i, :) = currentSolution; % 当前全体城市排序
        CandsList(i, [A(i, 1), A(i, 2)]) = currentSolution([A(i, 2), A(i, 1)]); % 交换位置
        F(i) = calculateDistance(CandsList(i, :), distanceMatrix); % 计算距离
    end

    % 对F从小到大排序
    [value, order] = sort(F);

    for i = 1:beCandsNum % 整理beCandsNum个最好邻域解
        beCands(i, 1) = order(i);
        beCands(i, 2) = value(i);
        beCands(i, 4) = A(order(i), 2);
        beCands(i, 3) = A(order(i), 1);
    end

    if beCands(1, 2) < bestFitnessValue % 无条件接受好解
        bestFitnessValue = beCands(1, 2); % 邻城解集中较小的目标值代替原最优值
        currentSolution = CandsList(beCands(1, 1), :); % 新的排序代替原最优排序
        bestSolution = currentSolution; % 记录当前最优排序
        updateHappen = 1; % 是否有更新标记，为节省画图时间
        TL = updateTabuList(TL, beCands(1, 3), beCands(1, 4), cityNum, TLLength);
    else % 接受劣解

        for i = 1:beCandsNum

            if TL(beCands(i, 3), beCands(i, 4)) == 0 % 如果这两个位置的交换没有被禁
                currentSolution = CandsList(beCands(i, 1), :); % 设为当前解
                updateHappen = 1; % 是否有更新标记
                TL = updateTabuList(TL, beCands(i, 3), beCands(i, 4), cityNum, TLLength);
                break;
            end

        end

    end

    currentTime = currentTime + 1;

    if updateHappen == 1 % 有变化标记，重画图；否则不再画
        displayResult(currentTime, bestSolution, bestFitnessValue, cityNum, city);
        updateHappen = 0;
    end

    pause(0.005);
end

DrawRoute(city, bestSolution) % 画路线图函数

% 计算各城市之间的距离矩阵函数
function distanceMatrix = getDistanceMatrix(cityNum, city)
    distanceMatrix = zeros(cityNum, cityNum);

    for i = 1:cityNum

        for j = 1:cityNum
            distanceMatrix(i, j) = ((city(i, 1) - city(j, 1)) ^ 2 + (city(i, 2) - city(j, 2)) ^ 2) ^ 0.5;
        end

    end

end

% 返回一组2-opt邻域交换位置的选择
function NB = Neiborhood(cityNum, neiborNum)
    ik = 1;
    NB = zeros(neiborNum, 2); % 保存邻域
    % 直到产生neborNum个邻域位置
    while ik <= neiborNum
        M = ceil(cityNum * rand(1, 2)); % 产生两个不重复的随机值

        if M(1) ~= M(2)
            NB(ik, 1) = min(M);
            NB(ik, 2) = max(M);

            if ik == 1 % 第一个邻域交换不需要检测
                isdel = 0;
            else % 其余邻域交换需要检测是否重复

                for jk = 1:ik - 1

                    if NB(ik, 1) == NB(jk, 1) && NB(ik, 2) == NB(jk, 2)
                        isdel = 1; % 建立重复标记
                        break;

                    else
                        isdel = 0;
                    end

                end

            end

            if ~isdel
                ik = ik + 1;
            else
                continue; % 若新产生的邻域交换与之前的重复，不计入
            end

        else
            continue; % 产生了两个相同的值，无效
        end

    end

end

% 计算解solution的距离
function distance = calculateDistance(solution, distanceMatrix)
    DistanV = 0;
    n = size(solution, 2);

    for i = 1:(n - 1)
        DistanV = DistanV + distanceMatrix(solution(i), solution(i + 1));
    end

    DistanV = DistanV + distanceMatrix(solution(n), solution(1));
    distance = DistanV;
end

% 更新禁忌表，加入新元素，其余遍历减1
function tabuList = updateTabuList(tabuList, x, y, cityNum, tabuListLength)

    for m = 1:cityNum

        for n = 1:cityNum

            if tabuList(m, n) ~= 0
                tabuList(m, n) = tabuList(m, n) - 1;
            end

        end

    end

    tabuList(x, y) = tabuListLength;
end

% 即时显示当前搜索到的最佳路径
function displayResult(currentTime, bestSolution, bestFitnessValue, cityNum, city)

    for i = 1:cityNum - 1
        plot([city(bestSolution(i), 1), city(bestSolution(i + 1), 1)], ...
            [city(bestSolution(i), 2), city(bestSolution(i + 1), 2)], 'bo-');
        hold on;
    end

    plot([city(bestSolution(cityNum), 1), city(bestSolution(1), 1)], ...
        [city(bestSolution(cityNum), 2), city(bestSolution(1), 2)], 'bo-');
    title(['Counter:', int2str(currentTime), 'The Min Distance:', num2str(bestFitnessValue)]);
    hold off;
end

% 画出最终路线图
function DrawRoute(C, R)
    N = length(R);
    scatter(C(:, 1), C(:, 2)); % 画出坐标散点
    set(gcf, 'color', 'none');
    alpha(0.1);
    hold on;
    plot([C(R(1), 1), C(R(N), 1)], [C(R(1), 2), C(R(N), 2)], 'g'); % 画出1~N之间连线
    hold on;

    for i = 2:N
        plot([C(R(i - 1), 1), C(R(i), 1)], [C(R(i - 1), 2), C(R(i), 2)], 'k'); % 逐条画出其他连线
    end

    hold on
    title('优化路径图');
end
