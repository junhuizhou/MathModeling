% 差分进化算法求解Rosenbrock函数
% min f(x1,x2) = (1-x1)^2 + 100*(x2-x1^2)^2
% 变量(x1,x2)在区间[-2.0, 2.0]
% 使用标准DE算法，随机选择三个异样个体，一个差分变量，二项式分布交叉
% 程序预留支持另外四种策略，由变量strategy=1~5分别指定
% 最优解为(x1.x2)=(1,1)

clear all;
close all;
clc;

% 参数初始化
fname = 'rosen'; % 用字符串命名函数。rosen函数以最小化为目标
VTR = 1.e-6; % 目标函数误差精度≤VTR后搜索停止
popSize = 20; % 种群规模
D = 2; % 变量维数
XVmin = [-2 -2]; % 变量定义域下限和上限
XVmax = [2 2];
iterMax = 200; % 最大迭代次数
F = 0.8; % 缩放因子，通常在[0, 2]范围内
CR = 0.8; % 交叉概率
strategy = 1; % 交叉策略选项，此处选1
pop = zeros(popSize, D);

for i = 1:popSize % 产生初始种群，限定在[XVmin，XVmax]
    pop(i, :) = XVmin + rand(1, D) .* (XVmax - XVmin);
end

popold = zeros(size(pop)); % 保存当前种群
val = zeros(1, popSize); % 初始化目标值向量均为0
ibest = 1;
val(1) = feval(fname, pop(ibest, :)); % 计算第一个个体目标值
bestval = Inf; % 并且设定为最佳目标值

for i = 2:popSize % 计算各个体的目标值并保留最优结果

    val(i) = feval(fname, pop(i, :));

    if (val(i) < bestval)
        ibest = i; % 保留最佳目标解和目标值
        bestval = val(i);
    end

end

bestmemit = pop(ibest, :); % 当前最佳解
bestvalit = bestval; % 当前最佳值
bestmem = bestmemit;

% 本程序支持五种差分更新策略，故从种群中随机选出5个个体参与更新
pml = zeros(popSize, D);
pm2 = zeros(popSize, D);
pm3 = zeros(popSize, D);
pm4 = zeros(popSize, D);
pm5 = zeros(popSize, D);
bm = zeros(popSize, D); % 最佳个体
ui = zeros(popSize, D); % 更新后的个体
rot = (0:1:popSize - 1); % 旋转索引数组（大小为popSize)
rotd = (0:1:D - 1); % 旋转索引数组（大小为D)
rt = zeros(popSize); % 另一个旋转索引数组
rtd = zeros(D); % 指数交叉的旋转索引数组

% 开始循环迭代
iter = 1;

while ((iter < iterMax) || (bestval > VTR))
    % 为了增加多样性对原种群进行混合旋转操作

    popold = pop; % 保存原种群
    ind = randperm(4); % 原种群做混合预处理
    a1 = randperm(popSize);
    rt = rem(rot + ind(1), popSize); % 求余数
    a2 = a1(rt + 1);
    rt = rem(rot + ind(2), popSize);
    a3 = a2(rt + 1);
    rt = rem(rot + ind(3), popSize);
    a4 = a3(rt + 1);
    rt = rem(rot + ind(4), popSize);
    a5 = a4(rt + 1);
    pml = popold(a1, :); % 混合种群1
    pm2 = popold(a2, :); % 混合种群2
    pm3 = popold(a3, :); % 混合种群3
    pm4 = popold(a4, :); % 混合种群4
    pm5 = popold(a5, :); % 混合种群5

    for i = 1:popSize % 最佳解初始化
        bm(i, :) = bestmemit;
    end

    mui = rand(popSize, D) < CR; % 随机产生屏蔽字
    mui = sort(mui'); % 转置

    for i = 1:popSize
        n = floor(rand * D);

        if n > 0
            rtd = rem(rotd + n, D);
            mui(:, i) = mui(rtd + 1, i); % 对每一个个体随机给出屏蔽字
        end

    end

    mui = mui'; % 转置回去
    mpo = mui < 0.5; % 全部翻转

    % 变异和交叉

    if (strategy == 1) % DE/rand/1/bin
        ui = pml + F * (pm2 - pm3); % 1差分
        ui = popold .* mpo + ui .* mui; % 二项式交叉
    elseif (strategy == 2) % DE/best/1/bin
        ui = bm + F * (pm1 -pm2); % 1差分
        ui = popold .* mpo + ui .* mui; % 二项式交叉
    elseif (strategy == 3) % DE/rand-to-best/1/bin
        ui = pm3 + F * (bm - popold) + F * (pml - pm2); % 1差分
        ui = popold .* mpo + ui .* mui; % 交叉
    elseif (strategy == 4) % DE/best/2/bin
        ui = bm + F * (pml - pm2 + pm3 - pm4); % 2差分
        ui = popold .* mpo + ui .* mui; % 二项式交叉
    elseif (strategy == 5) % DE/rand/2/bin
        ui = pml + F * (pm2 - pm3 + pm4 - pm5); % 2差分
        ui = popold .* mpo + ui .* mui; % 二项式交叉
    end

    % 选择

    for i = 1:popSize
        tempval = feval(fname, ui(i, :));

        if (tempval <= val(i)) % 保留个体最佳值
            pop(i, :) = ui(i, :);
            val(i) = tempval;

            if (tempval < bestval)
                bestval = tempval;
                bestmem = ui(i, :);
            end

        end

    end

    bestmemit = bestmem; % 更新全局最佳解
    iter = iter + 1;
end % 循环迭代结束

% 输出结果
disp('最佳目标值:');
bestval
disp('最佳目标解:');
bestmem

% 计算目标函数值
function result = rosen(x)
    result = 100 * (x(2) - x(1) ^ 2) ^ 2 + (1 - x(1)) ^ 2;
end
