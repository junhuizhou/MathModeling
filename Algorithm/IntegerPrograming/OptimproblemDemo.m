% 使用optimproblem()求解混合整数线性规划问题
%例  max   z=3*x1+2*x2+x3
%    s.t. x1+x2+x3<=7
%         4*x1+2*x2+x3=12
%         x1,x2>=0
%         x3 binary

clear all;
clc;
%参数设置(由于部分变量为整数约束，分开处理)
%创建决策变量
x = optimvar('x',2,'LowerBound',0);
xb = optimvar('xb','LowerBound',0,'UpperBound',1,'Type','integer');
%创建线性规划问题
prob = optimproblem('ObjectiveSense', 'min');
prob.Objective = -3*x(1)-2*x(2)-xb;
%创建线性约束
cons1 = sum(x) + xb <= 7;
cons2 = 4*x(1) + 2*x(2) + xb == 12;
prob.Constraints.cons1 = cons1;
prob.Constraints.cons2 = cons2;
%问题转换结构
problem = prob2struct(prob);
%求解
[sol,fval,exitflag,output] = intlinprog(problem);
xmax = sol
zmax = -fval
