% 使用OPTI toolbox求解混合整数非线性规划问题MINP
%例  min  z=x1*x4*(x1+x2+x3)+x3
%    s.t. x1*x2*x3*x4>=25
%         x1^2+x2^2+x3^2+x4^2=40
%         1<=xi<=5
%         x1为整数

clear all;
clc;
%参数设置
cl = [25; 40];      %约束条件下界
cu = [inf; 40];     %约束条件上界
lb = ones(4,1);     %变量下界
ub = 5*ones(4,1);   %变量上界
x0 = [1 5 5 1];     %初始值估计
xtype = 'ICCC';     %x1为整数，其余为实数
fun = @obj;         %设置目标函数
nlcon = @cons;      %设置约束函数
%求解器参数设置(OPTI中只有少数求解器可以解MINP)
%Eg: opts = optiset('solver','BONMIN','display','iter');
opts = optiset('display', 'iter');
%创建求解对象
Opt = opti('fun',fun,'nl',nlcon,cl,cu,'bounds',lb,ub,'x0',x0,'xtype',xtype,'options',opts);
%求解
[x,fval,exitflag,output] = solve(Opt);
xmin = x
zmin = fval

%目标函数
function y = obj(x)
    y = x(1) * x(4) * (x(1)+x(2)+x(3)) + x(3);
end
%约束条件
function y = cons(x)
    y(1) = x(1)*x(2)*x(3)*x(4);
    y(2) = x(1)^2 + x(2)^2 + x(3)^2 + x(4)^2;
end
