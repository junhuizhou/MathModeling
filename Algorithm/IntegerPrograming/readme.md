<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-17 17:26:53
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-19 11:36:21
 * @FilePath: \MathModeling\Algorithm\IntegerPrograming\readme.md
 * @Description: file content
-->

# 整数规划的Matlab实现

## 求解方法
* 分支定界法——可求解完全或混合整数线性规划
* 割平面法——可求解完全或混合整数线性规划
* 隐枚举法——求解0-1整数规划
* 匈牙利法——求解指派问题
* 蒙特卡洛法——求解各种类型规划

## 使用函数
* intlinprog()——混合整数线性规划MILP包括0-1整数规划
* optimproblem()——调用intlinprog()求解
* OPTI toolbox ——其中部分求解器可以求解混合整数非线性规划MINP
* 其他未使用的CPLEX、IPSOLVE、MOSEK、SCIP、SEDUMI
