% 使用intlinprog()求解混合整数线性规划问题
%例  max   z=3*x1+2*x2+x3
%    s.t. x1+x2+x3<=7
%         4*x1+2*x2+x3=12
%         x1,x2>=0
%         x3 binary

clear all;
clc;
%参数设置
C = [-3; -2; -1];   %目标函数min
A = [1, 1, 1];      %不等式约束左边
b = 7;              %不等式约束右边
Aeq = [4, 2, 1];    %等式约束左边
beq = 12;           %等式约束右边
lb = zeros(3,1);    %下界
ub = [inf; inf; 1]; %上界，x3为0/1
intcon = 3;         %整数约束，x3为整数
%求解
[x, fval] = intlinprog(C, intcon, A, b, Aeq, beq, lb, ub);
xmax = x
zmax = -fval
