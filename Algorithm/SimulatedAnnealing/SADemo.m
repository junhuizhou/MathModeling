% 使用模拟退火算法求解TSP问题
% 邻域移动的产生：2-opt操作，即交换两点位置的值
% 温度初始值T0：依据实验结果进行若干次调整
% 温度下降取比率法，τ=0.95，也可根据实际调整

clear all;
close all;
clc;

cityNum = 20;
Coord = [8.54, 0.77, 17.02, 0.55, 18.47, 0.61, 10.36, 8.39, 4.85, 17.08, ...
             3.38, 9.59, 7.01, 16.62, 10.84, 2.58, 5.02, 5.78, 17.33, 7.43;
         4.15, 2.52, 4.41, 12.03, 0.70, 11.51, 16.24, 4.47, 1.63, 13.80, ...
             11.28, 4.66, 8.82, 12.65, 5.22, 9.67, 16.23, 6.34, 6.51, 0.55]; % 城市坐标
T0 = 1000; % 初温
tau = 0.95; % 降温系数τ
Ts = 1; % 终止温度
MaxInnerLoop = 50; % 内循环最大迭代次数
neiborNum = cityNum; % 邻域解最大数量
fare = distance(Coord); % 计算距离对称矩阵
path = randperm(cityNum); % 随机生成一个初始解
pathfar = pathfare(fare, path); % 计算初始路径长度
bestValue = pathfar; % 全局最好目标值
currentbestValue = bestValue; % 当前最好目标值
bestPath = path; % 全局最好路径
currentbestPath = path; % 当前最好路径

% 达到终止温度时结束
while T0 >= Ts

    for in = 1:MaxInnerLoop % 内循环模拟等温过程
        e0 = pathfare(fare, path); % 从当前解开始
        NborNum = Neiborhood(cityNum, neiborNum); % 返回一组邻域
        swapDone = swap(path, neiborNum, NborNum); % 完成邻域交换
        e2 = pathfare(fare, swapDone); % 计算新邻域交换后的目标值
        [better, index] = sort(e2); % 目标值按照升序排序
        e1 = better(1, 1); % 第一个值是最好的
        newpath = swapDone(index(1), :); % 本轮迭代最好解

        if e1 < e0 % 目标值更好，无条件接受
            currentbestValue = e1;

            currentbestPath = newpath;
            path = newpath; % 把当前最好点设为下一轮起始点

            if bestValue > currentbestValue % 保留全局最好值
                bestValue = currentbestValue;
                bestPath = currentbestPath;
            end

        else % 按照Metropolis 准则有条件接受
            pt = min(1, exp(- (e1 - e0) / T0));

            if pt > rand
                path = newpath; % 接受劣解
                e0 = e1;

            end

        end

    end % 内循环结束

    T0 = tau * T0; % 降温
    displayResult(i, bestPath, bestValue, cityNum, Coord'); % 画图显示输出
    pause(0.005);
end % 外循环结束

bestPath
bestValue

% 产生一组2-opt邻域交换位置
function NB = Neiborhood(cityNum, neiborNum)
    ik = 1;
    NB = zeros(neiborNum, 2); % 保存邻域
    % 直到产生neiborNum个邻域位置
    while ik <= neiborNum
        M = ceil(cityNum * rand(1, 2)); % 产生两个不重复的随机值

        if M(1) ~= M(2)
            NB(ik, 1) = min(M);
            NB(ik, 2) = max(M);

            if ik == 1 % 第一个邻域交换不需要检测
                isdel = 0;
            else % 其余邻域交换需要检测是否重复

                for jk = 1:ik - 1

                    if NB(ik, 1) == NB(jk, 1) && NB(ik, 2) == NB(jk, 2)
                        isdel = 1; % 建立重复标记
                        break;
                    else
                        isdel = 0;
                    end

                end

            end

            if ~isdel
                ik = ik + 1;
            else
                continue; % 若新产生的邻域交换与之前的重复，不计入
            end

        else
            continue; % 产生了两个相同的值，无效
        end

    end

end

% 完成邻域交换
% 实现oldpath互换操作
% number为产生的新路径的个数
% position 为对应newpath互换的位置
function newpath = swap(oldpath, number, position)
    m = length(oldpath); % 城市的个数
    newpath = zeros(number, m);

    for i = 1:number
        newpath(i, :) = oldpath; % 交换路径中选中的城市
        newpath(i, position(i, 1)) = oldpath(position(i, 2));
        newpath(i, position(i, 2)) = oldpath(position(i, 1));
    end

end

% 计算路径path的长度
% path为1到n的排列，代表城市的访问顺序
% fare为路径长度矩阵，且为对称方阵
function objval = pathfare(fare, path)
    [m, n] = size(path); objval = zeros(1, m);

    for i = 1:m

        for j = 2:n
            objval(i) = objval(i) + fare(path(i, j - 1), path(i, j));
        end

        objval(i) = objval(i) + fare(path(i, n), path(i, 1));
    end

end

% 计算距离矩阵
function fare = distance(coord)
    [n, m] = size(coord); % m 为城市的个数
    fare = zeros(m);

    for x = 1:m % 外层为行

        for y = x:m % 内层为列
            fare(x, y) = (sum((coord(:, x) - coord(:, y)) .^ 2)) ^ 0.5;
            fare(y, x) = fare(x, y); % 距离矩阵对称
        end

    end

end

% 显示输出路径的变化
function displayResult(currentTime, bestSolution, bestFitnessValue, cityNum, city)

    for i = 1:cityNum - 1
        plot([city(bestSolution(i), 1), city(bestSolution(i + 1), 1)], ...
            [city(bestSolution(i), 2), city(bestSolution(i + 1), 2)], 'bo-');
        hold on;
    end

    plot([city(bestSolution(cityNum), 1), city(bestSolution(1), 1)], ...
        [city(bestSolution(cityNum), 2), city(bestSolution(1), 2)], 'bo-');
    title(['Counter:', int2str(currentTime), 'The Min Distance:', num2str(bestFitnessValue)]);
    hold off;
end
