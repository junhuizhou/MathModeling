% 使用贪心随机自适应搜索算法(greedy randomized adaptive search procedure, GRASP)求解17个城市的TSP问题
% 目标最优解2085

clear all;
close all;
clc;

gr17Matrix = [
              0, 633, 257, 91, 412, 150, 80, 134, 259, 505, 353, 324, 70, 211, 268, 246, 121
              633, 0, 390, 661, 227, 488, 572, 530, 555, 289, 282, 638, 567, 466, 420, 745, 518
              257, 390, 0, 228, 169, 112, 196, 154, 372, 262, 110, 437, 191, 74, 53, 472, 142
              91, 661, 228, 0, 383, 120, 77, 105, 175, 476, 324, 240, 27, 182, 239, 237, 84
              412, 227, 169, 383, 0, 267, 351, 309, 338, 196, 61, 421, 346, 243, 199, 528, 297
              150, 488, 112, 120, 267, 0, 63, 34, 264, 360, 208, 329, 83, 105, 123, 364, 35
              80, 572, 196, 77, 351, 63, 0, 29, 232, 444, 292, 297, 47, 150, 207, 332, 29
              134, 530, 154, 105, 309, 34, 29, 0, 249, 402, 250, 314, 68, 108, 165, 349, 36
              259, 555, 372, 175, 338, 264, 232, 249, 0, 495, 352, 95, 189, 326, 383, 202, 236
              505, 289, 262, 476, 196, 360, 444, 402, 495, 0, 154, 578, 439, 336, 240, 685, 390
              353, 282, 110, 324, 61, 208, 292, 250, 352, 154, 0, 435, 287, 184, 140, 542, 238
              324, 638, 437, 240, 421, 329, 297, 314, 95, 578, 435, 0, 254, 391, 448, 157, 301
              70, 567, 191, 27, 346, 83, 47, 68, 189, 439, 287, 254, 0, 145, 202, 289, 55
              211, 466, 74, 182, 243, 105, 150, 108, 326, 336, 184, 391, 145, 0, 57, 426, 96
              268, 420, 53, 239, 199, 123, 207, 165, 383, 240, 140, 448, 202, 57, 0, 483, 153
              246, 745, 472, 237, 528, 364, 332, 349, 202, 685, 542, 157, 289, 426, 483, 0, 336
              121, 518, 142, 84, 297, 35, 29, 36, 236, 390, 238, 301, 55, 96, 153, 336, 0];
[~, probSize] = size(gr17Matrix); % 取得问题维数（变量个数）
iterations = 200; % 最大迭代次数
alpha = 0.5; % α值：较小的α偏向贪心，较大的α偏向随机策略
count = 0; % 记录迭代次数
bestValue = Inf; % 记录最佳目标值
best_sol = zeros(1, probSize + 1); % 记录最佳解
repeatLocal = 5;
repeatRCL = 5;

while count < iterations

    for i = 1:repeatRCL % 重复RCL拽索repeatRCL次
        [rcl_list, sol] = RCL(gr17Matrix, alpha, probSize); % 贪心随机启发式搜索

        if rcl_list < bestValue
            bestValue = rcl_list;
            best_sol = sol;
        end

    end

    forj = 1:repeatLocal % 重复局部搜索repeatLocal次
    [bestV, solution] = local_search(gr17Matrix, best_sol);

    if bestV < bestValue
        bestValue = bestV; % 总是保留最好值
        best_sol = solution;
    end

    count = count + 1;
end

bestValue
best_sol

% 贪心随机搜索，构造RCL
function [RCL, sequence] = RCL(Xdata, alpha, probSize)
    start = randperm(probSize, 1); % 产生一个随机初始点
    sequence = [start]; % 构造起点

    while length(sequence) < probSize
        rand1 = rand();

        if rand1 > alpha % 贪心选择

            if sequence(end) == 1 % 排除节点为1的值
                city = 1;
            else
                city = sequence(end) - 1;
            end

            rank = ranking(Xdata, city, probSize); % 返回当前节点序列
            [~, I] = sort(rank(:, 1));
            rank = rank(I, :); % 按照第一列(距离）升序排序
            count = 0; % 选择最小的值
            next_city = rank(1, 2); % 初始化下一个节点
            % 若已在当前解序列予以排除
            while ismember(next_city, sequence)
                count = count + 1;
                next_city = rank(count, 2);
            end

            sequence = [sequence, next_city]; % 把下一个最好的节点加入解序列
        else % 随机选择
            next_city = randperm(probSize, 1); % 随机产生初始节点
            % 若已在当前解序列予以排除
            while ismember(next_city, sequence)
                next_city = randperm(probSize, 1); % 随机生成下一个节点
            end

            sequence = [sequence, next_city]; % 把下一个节点加入解序列
        end

    end

    sequence = [sequence, sequence(1)]; % 回到原点
    RCL = distance_calc(Xdata, sequence); % 计算解
end

% 局部搜索
function [currentBestV, currentBestSol] = local_search(Xdata, city_tour)
    currentBestSol = city_tour; % 初始化
    best_route = city_tour;
    currentBestV = distance_calc(Xdata, best_route);
    matrixSize = length(best_route) - 1;
    % 循环邻域交换搜索
    for i = 1:matrixSize - 2

        for j = 1:matrixSize - 1
            best_route(1, i:j + 1) = flip(best_route(1, i:j + 1));
            best_route(1, end) = best_route(1, 1);
            calResult = distance_calc(Xdata, best_route);

            if calResult < currentBestV
                currentBestV = calResult; % 保留当前最佳结果
                currentBestSol = best_route;
            end

            best_route = city_tour; % 恢复原始解
        end

    end

end

% 按照距离对当前节点排序
function rank = ranking(Xdata, city, probSize)
    rank = zeros(probSize, 2);
    rank(:, 1) = Xdata(:, city);
    rank(:, 2) = 1:probSize;
end

% 计算目标函数值
function distanV = distance_calc(Xdata, city_tour)
    distanV = 0;
    n = size(city_tour, 2);

    for i = 1:(n - 1)
        distanV = distanV + Xdata(city_tour(i), city_tour(i + 1));
    end

    distanV = distanV + Xdata(city_tour(n), city_tour(1)); % 回到出发点
end
