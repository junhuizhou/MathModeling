% 使用迭代局部搜索算法(iterated local search, ILS)求解Griewank标准测试函数的最小值
% Griewank具有许多广泛分布的局部极小值，函数最小值时0，最优解为(0,0,...,0)

clear all;
close all;
clc;

fname = @griewank;
Maxiter = 10000;
NDim = 30; % 变量个数d
Bound = [-100, 100]; % 变量范围
iteration = 0; % 迭代次数
Popsize = 30; % 种群规模，初始化时用于找到一个好的初始解
rdp = 0.7; % 对局部解进行扰动，扰动概率为0.7，可以调整
numLocalSearch = 5; % 局部搜索次数
numPerturbation = 10; % 扰动次数
Lowerbound = zeros(NDim, Popsize); % 变量下限初始化
Upperbound = zeros(NDim, Popsize); % 变量上限初始化

for i = 1:Popsize
    Lowerbound(:, i) = Bound(1); % 设定变量下限值
    Upperbound(:, i) = Bound(2); % 设定变量上限值
end

Population = Lowerbound + rand(NDim, Popsize) .* (Upperbound - Lowerbound); % 给定随机初始解

for i = 1:Popsize
    fvalue(i) = fname(Population(:, i)); % 计算初始群函数值
end

[fvaluebest, index] = min(fvalue); % 找到最小值
Populationbest = Population(:, index); % 最优解
prefvalue = fvalue;

% 迭代局部寻优
while (iteration < Maxiter)
    iteration = iteration + 1;

    for i = 1:numLocalSearch % 做多次局部搜索
        a = Populationbest -1/10 .* (Populationbest - Lowerbound(:, i)); % 局部搜索的下限
        b = Populationbest +1/10 .* (Upperbound(:, i) - Populationbest); % 局部搜索的上限
        numPerturbation = 10; % 对每一个解做局部随机搜索的次数
        Population_new = zeros(NDim, numPerturbation); % 局部随机搜索的群体

        for j = 1:numPerturbation % 做多次随机扰动
            Population_new(:, j) = Populationbest;
            change = rand(NDim, 1) < rdp;
            Population_new(change, j) = a(change) + (b(change) - a(change)) .* rand(1);
            fvalue_new(j) = fname(Population_new(:, j)); % 计算目标值
        end

        [fval_newbest, index_new] = min(fvalue_new);

        if fval_newbest < fvaluebest
            fvaluebest = fval_newbest;
            Populationbest = Population_new(:, index_new);
            break; % 已经找到更好解，进入下一轮迭代
        end

    end

end

% 输出解值
Populationbest % 目标最优解
fvaluebest % 目标最优值

% 计算griewank函数
% INPUT: XX= [x1, x2,... xd]
function [y] = griewank(xx)
    d = length(xx);
    sum = 0;
    prod = 1;

    for ii = 1:d
        xi = xx(ii);
        sum = sum + xi ^ 2/4000;
        prod = prod * cos(xi / sqrt(ii));
    end

    y = sum - prod + 1;
end
