% 使用变邻域下降算法(variable neighborhood descent, VND)求解12个城市的TSP问题
% 算法使用3种邻域结构

clear all;
close all;
clc;

iterxMax = 5; % 最大迭代次数
iterx = 0;
distmat = [% 距离矩阵
           0 350 290 670 600 500 660 440 720 410 480 970
           350 0 340 360 280 375 555 490 785 760 700 1100
           290 340 0 580 410 630 795 680 1030 695 780 1300
           670 360 580 0 260 380 610 805 870 1100 1000 1100
           600 280 410 260 0 610 780 735 1030 1000 960 1300
           500 375 630 380 610 0 160 645 500 950 815 950
           660 555 795 610 780 160 0 495 345 820 680 830
           440 490 680 805 735 645 495 0 350 435 300 625
           720 785 1030 870 1030 500 345 350 0 475 320 485
           410 760 695 1100 1000 950 820 435 475 0 265 745
           480 700 780 1000 960 815 680 300 320 265 0 585
           970 1100 1300 1100 1300 950 830 625 485 745 585 0];
length = size(distmat, 1); % 测量城市个数12个
currentSolution = randperm(length); % 随机产生初始解
shorterDistance = CalDist(distmat, currentSolution); % 计算初始解并赋为最佳值

while iterx < iterxMax
    currentSolution = randperm(length); % 随机产生邻域
    [currentDistance, currentSolution] = VND(currentSolution, distmat); % VND

    if currentDistance < shorterDistance
        shorterDistance = currentDistance; % 始终保存最佳路径长度
        bestSolution = currentSolution; % 保存最佳路径
        iterx = 0;
    else
        iterx = iterx + 1;
    end

end

shorterDistance % 输出结果
bestSolution

% VND 算法
function [dis, solution] = VND(solution, dislist)
    % 选用三种邻域结构展开搜索
    dis = inf;
    k = -1;
    Imax = 3; % 三种邻域结构
    i = 1;

    while i <= Imax

        switch (i)
            case (1)
                neiborSolution = neighborhoodOne(solution); % swap 算子
            case (2)
                neiborSolution = neighborhoodTwo(solution); % two_opt_swap 算子
            case (3)
                neiborSolution = neighborhoodThree(solution); % two h_opt_swap 算子
        end

        neighborNum = size(neiborSolution, 1);

        for j = 1:neighborNum
            temp = CalDist(dislist, neiborSolution(j, :));

            if temp < dis
                dis = temp;
                k = j;

            end

        end

        if dis < CalDist(dislist, solution)
            solution = neiborSolution(k, :); % 转第一个邻域结构
            i = 1;
        else

            i = i + 1; % 转下一个邻域结构
        end

    end

end

% 邻域结构1：swap算子
function neighbor = neighborhoodOne(sol)
    len = length(sol);
    count = 1:len - 1;

    neighborNum = sum(count); % 邻域个数
    neighbor = zeros(neighborNum, len);
    k = 0;

    for i = 1:len

        for j = i + 1:len
            k = k + 1;
            s = sol;
            x = s(:, j);
            s(:, j) = s(:, i);
            s(:, i) = x; % 产生新的邻域解
            neighbor(k, :) = s; % 存储新的邻域解
        end

    end

end

% 邻域结构2：two_opt_swap 算子
function neighbor = neighborhoodTwo(sol)
    len = length(sol);
    step = 3;
    count = 1:len - step; % 剩余步长
    neighborNum = sum(count); % 邻域个数
    neighbor = zeros(neighborNum, len);
    k = 0;

    for i = 1:len

        for j = i + 3:len
            k = k + 1;
            s = sol;
            s1 = s(i:j);
            s1 = fliplr(s1); % 反转
            s = [s(1:i - 1) s1 s(j + 1:end)]; % 产生新的邻域解
            neighbor(k, :) = s; % 存储新的邻域解
        end

    end

end

% 邻域结构3：two_h_opt_swap 算子
function neighbor = neighborhoodThree(sol)
    len = length(sol);
    count = 1:len - 1;
    neighborNum = sum(count); % 邻域个数

    neighbor = zeros(neighborNum, len);
    k = 0;

    for i = 1:len

        for j = i + 1:len
            k = k + 1;
            s = sol;
            s = [s(i) s(j) s(1:i - 1) s(i + 1:j - 1) s(j + 1:end)]; % 产生新的邻域解
            neighbor(k, :) = s; % 存储新的邻域解
        end

    end

end

% =
% 计算距离
function len = CalDist(dismat, solution)
    [soluNum, soluSize] = size(solution);
    len = zeros(1, soluNum);

    for i = 1:soluNum
        R = solution(i, :);

        for j = 1:(soluSize - 1)
            len(i) = len(i) + dismat(R(j), R(j + 1)); % dismat 是距离矩阵
        end

        len(i) = len(i) + dismat(R(1), R(soluSize)); % 回到起始点走过的距离
    end

end
