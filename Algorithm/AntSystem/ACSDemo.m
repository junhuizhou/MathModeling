% 使用蚁群算法(ant colony system, ACS)求解TSP问题
% 相对于AS算法做了4处改进(加快收敛)
% 增加了使用随机比例规则进行状态转移
% 修改了全局信息素更新规则
% 修改了局部信息素更新规则
% 建立了候选集策略

clear all;
close all;
clc;

% 第一步，参数初始化
City = [8.54, 0.77, 17.02, 0.55, 18.47, 0.61, 10.36, 8.39, 4.85, 17.08, ...
            3.38, 9.59, 7.01, 16.62, 10.84, 2.58, 5.02, 5.78, 17.33, 7.43;
        4.15, 2.52, 4.41, 12.03, 0.70, 11.51, 16.24, 4.47, 1.63, 13.80, ...
            11.28, 4.66, 8.82, 12.65, 5.22, 9.67, 16.23, 6.34, 6.51, 0.55]; % 城市坐标
City = City';
n = size(City, 1); % 城市数量
m = n; % 蚂蚁个数
NC_max = 200; % 最大循环迭代次数。
Alpha = 1; % 反映蚂蚁信息素的重要程度
Beta = 5; % 反映启发式信息的重要程度
Rho = 0.5; %反映路径上信息素的挥发程度
R_best = zeros(NC_max, n); % 最佳路径初始化为0
L_best = inf .* ones(NC_max, 1); % 最佳路径长度初始化为0
Tau = ones(n, n); % τ残留信息素更新矩阵，规模为nXn
Tabu = zeros(m, n); % 用于存储路径节点编号，第i只蚂蚁，第j个节点
NC = 1; % 迭代次数

% 计算距离矩阵
D = zeros(n, n);

for i = 1:n

    for j = 1:n

        if i ~= j
            D(i, j) = ((City(i, 1) - City(j, 1)) ^ 2 + (City(i, 2) - City(j, 2)) ^ 2) ^ 0.5;
        else
            D(i, j) = 0;
        end

        D(j, i) = D(i, j); % 对称矩阵

    end

end

Eta = 1 ./ D; % Eta为期望因子矩阵，取距离D(ij)的倒数

% 第二步：将m只蚂蚁随机放到n个节点上，每只蚂蚁出发的节点不同
while NC <= NC_max
    Randpos = randperm(n); % 随机产生个不重复整数值
    Tabu(:, 1) = (Randpos(1, 1:m))'; % 取前m个值为每只蚂蚁出发节点

    % 第三步：m只蚂蚁按转移概率选择下一个节点
    for j = 2:n % 出发节点不计算，共有n-1个需要爬过

        for ant_i = 1:m % m只蚂蚁逐一开始构造路径

            visited = Tabu(ant_i, 1:(j-1)); % 记录已访问的节点避免重复访问
            P = zeros(1, (n - j + 1)); % 记录未访问节点的选择概率
            unvisited = 1:n;
            unvisited = setdiff(unvisited, visited); % 仅剩未访问的节点
            q0 = 0.5; % q0∈[0,1]，较大值侧重于开发，较小值侧重于探索

            if rand <= q0 % 如果q<=q0

                for k = 1:length(unvisited)
                    P(k) = (Tau(visited(end), unvisited(k))) ^ Alpha * (Eta(visited(end), unvisited(k))) ^ Beta;
                end

                position = find(P == max(P)); % 选最大值
                next_to_visit = unvisited(position(1)); % 选定下一个节点

            else % 如果q>q0

                for k = 1:length(unvisited)
                    P(k) = (Tau(visited(end), unvisited(k))) ^ Alpha * (Eta(visited(end), unvisited(k))) ^ Beta;
                end

                P = P / sum(P); % 状态转移规则
                pcum = cumsum(P);
                select = find(pcum >= rand);
                next_to_visit = unvisited(select(1)); % 选定下一个节点
            end

            Tabu(ant_i, j) = next_to_visit; % 计入禁止访问节点矩阵
        end

    end

    % 这种写法，本轮最优就是全局最优
    if NC >= 2 % 第一轮迭代时无须记录
        Tabu(1, :) = R_best(NC - 1, :); % 记录每一轮迭代的最佳路径
    end

    % 第四步：计算距离，并记录本次迭代最佳路线和距离
    L = zeros(m, 1);

    for i = 1:m

        R = Tabu(i, :);

        for j = 1:(n - 1)
            L(i) = L(i) + D(R(j), R(j + 1));
        end

        L(i) = L(i) + D(R(1), R(n));

    end

    L_best(NC) = min(L); % 记录本轮最短路径长度
    pos = find(L == L_best(NC)); % 记录最短路径蚂蚁位置
    R_best(NC, :) = Tabu(pos(1), :); % 记录每次迭代后的最短路径

    % 第五步：更新信息素，采用ACS全局信息素更新规则
    Delta_Tau = zeros(n, n);

    for j = 1:(n - 1)
        % 只在全局最优的路径上应用更新信息素残留
        Delta_Tau(R_best(NC, j), R_best(NC, j + 1)) = Delta_Tau(R_best(NC, j), R_best(NC, j + 1)) + 1 / L_best(NC);
    end

    % 回到出发点
    Delta_Tau(R_best(NC, n), R_best(NC, 1)) = Delta_Tau(R_best(NC, n), R_best(NC, 1)) + 1 / L_best(NC);
    Tau = (1 - Rho) .* Tau + Rho * Delta_Tau; % 信息素挥发因子更新信息素式

    % 第六步：禁忌表清零
    Tabu = zeros(m, n);
    NC = NC + 1;
end

% 第七步：输出结果
Pos = find(L_best == min(L_best)); % 找到最佳路径（非0为真）
Shortest_Route = R_best(Pos(1), :); % 最大迭代次数后最佳路径
Shortest_Length = L_best(Pos(1)) % 最大迭代次数后最短距离
DrawRoute(City, Shortest_Route); % 画路线图函数
title('最短距离');

% 画出路径图
function DrawRoute(C, R)
    N = length(R);
    scatter(C(:, 1), C(:, 2)); % 坐标散点
    hold on;
    plot([C(R(1), 1), C(R(N), 1)], [C(R(1), 2), C(R(N), 2)], 'g'); % 画出连线
    hold on;

    for i = 2:N
        plot([C(R(i - 1), 1), C(R(i), 1)], [C(R(i - 1), 2), C(R(i), 2)], 'g'); % 画出连线
        hold on;
    end

    title('TSP优化效果图');
end
