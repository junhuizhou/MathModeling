% 使用布谷鸟搜索算法求解Rosenbrock函数
% heavisideSymbolic Math Toolbox

clear all;
close all;
clc;

N = 20; % 种群规模
D = 5; % 问题维度2~20
T = 1000; % 迭代次数
Xmax = 5; % 问题变量取值范围
Xmin = -5;
Pa = 0.20; % 抛弃概率
bestValue = Inf; % 最佳目标值
nestPop = rand(N, D) * (Xmax - Xmin) + Xmin; % 随机生成n个鸟巢的初始位置
trace = zeros(1, T); % 存储每次迭代中的最优解

% 开始循环迭代
for t = 1:T

    levy_nestPop = levy(nestPop, Xmax, Xmin); % 莱维飞行产生新的位置
    % 莱维飞行后，替换更新鸟巢位置
    index = find(fitness(nestPop) > fitness(levy_nestPop));
    nestPop(index, :) = levy_nestPop(index, :);
    % 以概率Pa丢弃部分解并产生随机新解
    rand_nestPop = nestPop + rand .* heaviside(rand(N, D) - Pa) .* (nestPop(randperm(N), :) - nestPop(randperm(N), :));
    rand_nestPop(find(nestPop > Xmax)) = Xmax;
    rand_nestPop(find(nestPop < Xmin)) = Xmin;
    % 依概率淘汰后，替换更新鸟巢位置
    index = find(fitness(nestPop) > fitness(rand_nestPop));
    nestPop(index, :) = rand_nestPop(index, :);
    % 保留最佳结果
    [bestV, index] = min(fitness(nestPop));

    if bestValue > bestV % 保留全局最优解和值
        bestValue = bestV;
        bestSolution = nestPop(index, :);
    end

    trace(t) = bestV; % 保存每次迭代的最优解
end

x = bestSolution; % 输出结果
y = bestValue;
figure;
plot(trace);
xlabel('迭代次数');
ylabel('适值');
title('适值变化曲线');

% 使用Levy flights 机制产生新解
function [result] = levy(nestPop, Xmax, Xmin)
    [N, D] = size(nestPop);
    % 按照Mantegna法则计算
    beta = 1.5;
    sigma_u = (gamma(1 + beta) * sin(pi * beta / 2) / (beta * gamma((1 + beta) / 2) * 2 ^ ((beta - 1) / 2))) ^ (1 / beta);
    sigma_v = 1;
    u = normrnd(0, sigma_u, N, D);
    v = normrnd(0, sigma_v, N, D);
    step = u ./ (abs(v) .^ (1 / beta));
    alpha = 0.01 .* (nestPop(randperm(N), :) - nestPop(randperm(N), :));
    nestPop = nestPop + alpha .* step;
    % 限制变量的上下界
    nestPop(find(nestPop > Xmax)) = Xmax;
    nestPop(find(nestPop < Xmin)) = Xmin;
    result = nestPop;
end

% 计算Rosenbrock目标函数
function y = fitness(xx)
    [~, dim] = size(xx);
    x1 = xx(:, 1:dim - 1);
    x2 = xx(:, 2:dim);

    if dim == 2
        y = 100 * (x2 - x1 .^ 2) .^ 2 + (1 - x1) .^ 2; % 两个变量
    else
        y = sum((100 * (x2 - x1 .^ 2) .^ 2 + (x1 - 1) .^ 2)); % 多个变量
    end

end
