% 进化规划算法求解二次函数y = 21.5+x1*sin(4*pi*x1)+x2*sin(20*pi*x2)的最大值
% 变量定义域：x1∈[-3.0, 12.1], x2∈[4.1, 5.8]
% 参考值x1=11.1737, x2=5.8367, y=38.8363

clear all;
close all;
clc;

% 参数初始化
miu = 100; % 初始群体
MaxIter = 1000; % 最大迭代次数
sigma_x1 = ones(1, miu); % 每个个体x1分量标准差初始值
sigma_x2 = ones(1, miu); % 每个个体x2分量标准差初始值
compute_fit = @(x1, x2)(21.5 +x1 .* sin(4 * pi .* x1) + x2 .* sin(20 * pi .* x2));
rand('state', sum(100 * clock)); % 重置随机数生成器
pop_x1 = zeros(1, miu); % 用于存放每代个体x1分量的值
pop_x2 = zeros(1, miu); % 用于存放每代个体x2分量的值

for j = 1:5 % 平均变异

    for i = 1:20
        pop_x1((j - 1) * 20 +i) = -0.3 + (i - 1) * 0.62 + rand * 0.62; % x1分为20等分
        pop_x2((j - 1) * 20 +i) = 4.1 + (j - 1) * 0.34 + rand * 0.34; % x2分为5等分
    end

end

fit_pop = compute_fit(pop_x1, pop_x2); % 计算初始种群的适应度
fit_output = sort(fit_pop, 'descend'); % 对初始种群的适应度进行降序排列

for g = 1:MaxIter

    pop_mutate_x1 = pop_x1 + sqrt(sigma_x1) .* normrnd(0, 1, [1, miu]); % 变异个体的x1分量

    for k = 1:miu
        % x1变异后在定义范围内
        while pop_mutate_x1(k) <- 0.3 || pop_mutate_x1(k) > 12.1
            pop_mutate_x1(k) = pop_x1(k) + sqrt(sigma_x1(k)) * normrnd(0, 1);
        end

    end

    pop_mutate_x2 = pop_x2 + sqrt(sigma_x2) .* normrnd(0, 1, [1, miu]); % 变异个体的x2分量

    for k = 1:miu
        % x2变异后在定义范围内
        while pop_mutate_x2(k) < 4.1 || pop_mutate_x2(k) > 5.8
            pop_mutate_x2(k) = pop_x2(k) + sqrt(sigma_x2(k)) * normrnd(0, 1);
        end

    end

    sigma_x1 = sigma_x1 + sqrt(sigma_x1) .* normrnd(0, 1, [1, miu]); % 变异后x1分量的标准差
    sigma_x1 = abs(sigma_x1);
    sigma_x2 = sigma_x2 + sqrt(sigma_x2) .* normrnd(0, 1, [1, miu]); % 变异后x2分量的标准差
    sigma_x2 = abs(sigma_x2);
    % 采用q竞争法选择个体组成新种群
    pop_temp_x1 = cat(2, pop_x1, pop_mutate_x1); % 父代及变异产生的子代个体x1分量
    pop_temp_x2 = cat(2, pop_x2, pop_mutate_x2); % 父代及变异产生的子代个体x2分量
    fit_temp = compute_fit(pop_temp_x1, pop_temp_x2); % 父代及变异产生的子代个体适值
    score = zeros(1, 2 * miu); % 父代以及子代个体得分

    for list = 1:2 * miu
        % 从1到200中随机选择90个不重复的数作为q竞争法选择的裁判的位置
        position = randperm(2 * miu, 0.9 * miu);
        judge_x1 = pop_temp_x1(position); % q竞争法选择裁判的x1分量
        judge_x2 = pop_temp_x2(position); % q竞争法选择裁判的x2分量
        fit_judge = compute_fit(judge_x1, judge_x2); % q竞争法选择出来裁判的适应度
        % 累加各个体的分数
        for m = 1:0.9 * miu

            if fit_temp(list) >= fit_judge(m)

                score(list) = score(list) + 1;
            end

        end

    end

    [score, location] = sort(score, 'descend'); % 分数降序排列并返回相应的位置
    pop_x1 = pop_temp_x1(location(1:miu)); % 挑选得分最好的前100个个体组成新群体
    pop_x2 = pop_temp_x2(location(1:miu));
    fit_pop = compute_fit(pop_x1, pop_x2); % 新种群的适应度
    fit_output = sort(fit_pop, 'descend'); % 对新种群的适应度进行降序排列
    fprintf('第%d代: \t%f\n', g, fit_output(1)); % 输出适应度最高的个体
end
