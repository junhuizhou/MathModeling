% 混合蛙跳算法SFLA求解Rosenbrock函数
% 算法在进行子群局部深度搜索时采用了子族群策略
% 该策略把50个个体分成5个子群，每个子群有10个个体，再从这10个个体中随机选择3个个体进行局部深度搜索（Step3）
% 需要注意的是，当选择7个自变量时，Rosenbrock函数有一个局部极值点。

clc;
clear;
close all;

CostFunction = @(x)Rosenbrock(x); % 目标函数 Rosenbrock
nVar = 7; % 变量个数
VarSize = [1 nVar]; % 变量下标范围
VarMin = -2; % 变量定义域
VarMax = 2;
MaxIt = 2000; % 最大迭代次数
nPopMemeplex = 10; % 子群规模
nPopMemeplex = max(nPopMemeplex, nVar + 1); % Nelder-Mead标准
nMemeplex = 5; % 子群数量
nPop = nMemeplex * nPopMemeplex; % 种群规模
I = reshape(1:nPop, nMemeplex, []); % SFLA参数赋初值
fla_params.q = max(round(0.3 * nPopMemeplex), 2); % 父代个数
fla_params.alpha = 3; % 孙群执行次数
fla_params.Lmax = 5; % 局部迭代次数
fla_params.sigma = 2; % 最小最大跳跃步长
fla_params.CostFunction = CostFunction; % 函数名称
fla_params.VarMin = VarMin; % 变量定义域
fla_params.VarMax = VarMax;
empty_individual.Position = []; % 个体青蛙初始值空间
empty_individual.Cost = []; % 个体青蛙初始解空间
pop = repmat(empty_individual, nPop, 1); % 初始化种群矩阵

for i = 1:nPop % 种群赋初值
    pop(i).Position = unifrnd(VarMin, VarMax, VarSize);
    pop(i).Cost = CostFunction(pop(i).Position);
end

pop = SortPopulation(pop); % 按照目标值升序排序
BestSol = pop(1); % 初始当前全局最优值和最优解
BestCosts = nan(MaxIt, 1); % 初始化用以保存历次迭代的最佳目标值
gBestval = Inf; % 保存全局最佳结果
gbestSolu = [];

for it = 1:MaxIt
    fla_params.BestSol = BestSol;
    Memeplex = cell(nMemeplex, 1); % 方便子群处理
    % 分子群执行蛙跳算法
    for j = 1:nMemeplex % 对每一个子群执行局部深度搜索
        Memeplex{j} = pop(I(j, :)); % 子群分组，直接取出第j个子群组数据
        Memeplex{j} = RunFLA(Memeplex{j}, fla_params); % 执行子群j
        pop(I(j, :)) = Memeplex{j}; % 结果更新到原来的子群
    end

    pop = SortPopulation(pop); % 升序排序
    BestSol = pop(1); % 更新本轮迭代的最佳结果
    BestCosts(it) = BestSol.Cost; % 保存每一轮迭代的最佳值，便于显示结果

    if BestSol.Cost < gBestval % 保存全局最好值
        gBestval = BestSol.Cost;
        gbestSolu = BestSol.Position;
    end

    disp(['Iteration', num2str(it), ':Best Cost = ', num2str(BestCosts(it)), ':Best Solu = ', num2str(BestSol.Position)]);
end

% 输出结果
gBestval
gbestSolu
figure;
% 输出目标值随着迭代次数下降的曲线
semilogy(BestCosts, 'LineWidth', 1);
xlabel('迭代次数');
ylabel('目标值');
grid on;

% 执行蛙跳算法的局部深度拽索
function pop = RunFLA(pop, params)
    q = params.q; % 父代数量
    alpha = params.alpha; % 孙群最大迭代次数
    Lmax = params.Lmax; % 子群最大迭代次数
    sigma = params.sigma; % 跳跃步长
    CostFunction = params.CostFunction; % 目标函数
    VarMin = params.VarMin; % 变量边界
    VarMax = params.VarMax;
    VarSize = size(pop(1).Position); % 变量规模
    BestSol = params.BestSol; % 全局最佳值Xg
    nPop = numel(pop); % 返回pop中元素个数，子群规模
    P = 2 * (nPop + 1 - (1:nPop)) / (nPop * (nPop + 1)); % Selection Probabilities
    LowerBound = pop(1).Position;
    UpperBound = pop(1).Position;

    for i = 2:nPop % 限定变量值范围
        LowerBound = min(LowerBound, pop(i).Position);
        UpperBound = max(UpperBound, pop(i).Position);
    end

    for it = 1:Lmax % 局域深度搜索Lmax次F
        L = RandSample(P, q); % 从子群随机选择q个解样本（整数位置）
        B = pop(L); % 提取q个样本的原始数据

        for k = 1:alpha % 子族群执行次数
            [B, SortOrder] = SortPopulation(B); % 样本升序排序
            L = L(SortOrder);
            ImprovementStep2 = false; % 与子群内比较后的改善标记false=改善
            Censorship = false; % 与全局比较后的改善标记 false=改善

            % 孙群内最好和最差个体的比较处理
            NewSol1 = B(end);
            Step = sigma * rand(VarSize) .* (B(1).Position - B(end).Position); % 式(12-2-1)变形
            NewSol1.Position = B(end).Position + Step; % 式(12-2-2)

            % 判断变量是否在定义域内
            if all(NewSol1.Position) >= VarMin && all(NewSol1.Position) <= VarMax
                NewSol1.Cost = CostFunction(NewSol1.Position); % 计算目标值

                if NewSol1.Cost < B(end).Cost % 结果比最差个体好予以替换
                    B(end) = NewSol1;
                else
                    ImprovementStep2 = true; % 结果没有改善，做出标记
                end

            else
                ImprovementStep2 = true; % 变量不在定义域内
            end

            if ImprovementStep2 % 全局最好与最差个体的比较处理
                NewSol2 = B(end);
                Step = sigma * rand(VarSize) .* (BestSol.Position - B(end).Position); % 式(12-2-3)
                NewSol2.Position = B(end).Position + Step; % 式(12-2-4)
                % 限定变量在定义域内
                if all(NewSol2.Position) >= VarMin && all(NewSol2.Position) <= VarMax
                    NewSol2.Cost = CostFunction(NewSol2.Position); % 计算目标值

                    if NewSol2.Cost < B(end).Cost % 结果比最差个体好予以替换
                        B(end) = NewSol2;

                    else
                        Censorship = true; % 变量不在定义域内
                    end

                else
                    Censorship = true; % 变量不在定义域内
                end

            end

            if Censorship % 随机替换最差个体的处理
                B(end).Position = unifrnd(LowerBound, UpperBound); % 随机产生个体
                B(end).Cost = CostFunction(B(end).Position); % 计算目标值
            end

        end

        % 转下一个子族群
        pop(L) = B; % 更新孙群

    end

end

% 从子群中随机选择q个解样本
function L = RandSample(P, q)

    if ~exist('replacement', 'var')
        replacement = false;
    end

    L = zeros(q, 1);

    for i = 1:q
        L(i) = randsample(numel(P), 1, true, P); %  matlab 提供的函数

        if ~replacement
            P(L(i)) = 0;
        end

    end

end

% 计算Rosenbrock目标函数值
function y = Rosenbrock(xx)
    [~, dim] = size(xx);
    x1 = xx(:, 1:dim - 1);
    x2 = xx(:, 2:dim);

    if dim == 2
        y = 100 * (x2 - x1 .^ 2) .^ 2 + (1 - x1) .^ 2; % 两个变量
    else
        y = sum((100 * (x2 - x1 .^ 2) .^ 2 + (x1 - 1) .^ 2))'; % 多个变量
    end

end

% 目标值升序排序
function [pop, SortOrder] = SortPopulation(pop)
    Costs = [pop.Cost];
    [~, SortOrder] = sort(Costs);
    pop = pop(SortOrder);
end
