% 求解完全背包问题，即每种物品数量不限
%例 背包        容量15
%   物品1 价值1 重量1  数量不限
%   物品2 价值2 重量1  数量不限
%   物品3 价值2 重量2  数量不限
%   物品4 价值4 重量12 数量不限

clear all;
clc;
%参数设置
valPack = 15;                   %背包容量
numGoods = 4;                   %物品数量
matGoods = zeros(numGoods, 2);  %物品价值重量矩阵
matGoods(1,:) = [1,1];
matGoods(2,:) = [2,1];
matGoods(3,:) = [2,2];
matGoods(4,:) = [4,12];
%创建存放每次拿取物品最优解的矩阵
matPackValue = zeros(numGoods,valPack);
%动态规划求解
for i = 1:numGoods              %每个物品一行
    for j = 1:valPack           %将背包按最小刻度划分为多个小包
        vi = matGoods(i,1);     %v为value
        wi = matGoods(i,2);     %w为weight
        if j < wi               %当前容量装不下物品i
            matPackValue(i,j) = matPackValue(i-1,j);
        elseif i == 1           %处理下公式越界的部分
            matPackValue(i,j) = vi;
        elseif j == wi          %处理下公式越界的部分
            matPackValue(i,j) = max(matPackValue(i-1,j),vi);
        else                    %递归公式(最后的i-1改成i了)
            matPackValue(i,j) = max(matPackValue(i-1,j),...
                                    vi+matPackValue(i,j-wi));
        end
    end
end
