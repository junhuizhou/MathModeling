<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-20 16:12:33
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-21 22:06:21
 * @FilePath: \MathModeling\Algorithm\DynamicPrograming\KnapsackProblem\readme.md
 * @Description: file content
-->

# 背包问题

* 动态规划求解

## 背包九讲

* [ ] 01背包问题
    >* [x] 二维矩阵实现
    >* [ ] 滚动数组实现
    >* [ ] 一维数组实现
* [ ] 完全背包问题
    >* [x] 二维数组实现
    >* [ ] 一维数组实现
* [ ] 多维背包问题
* [ ] 混合背包问题
* [ ] 二维费用背包问题
* [ ] 分组背包问题
* [ ] 背包问题求解方案数
* [ ] 求背包问题的方案
* [ ] 有依赖的背包问题
