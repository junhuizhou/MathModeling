<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-19 21:42:33
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-20 11:13:57
 * @FilePath: \MathModeling\Algorithm\DynamicPrograming\readme.md
 * @Description: file content
-->

# 动态规划Matlab实现

* 以背包问题为例：
* 动态规划只考虑拿走整件商品或者不拿，处理不了只拿一部分的问题
    >* 毕竟动态规划的表格刻度不能无限化小，否则将导致表格太大
    >* 贪婪算法可以轻松处理这种只拿一部分的问题
* 动态规划要求每个子问题是离散的，且不能互相依赖
    >* 一个子问题取值受其他子问题影响的情况，动态规划解决不了
* 显然最优解时背包可能存在剩余空间却装不下其他东西


## 可解决问题

>* 背包问题
>* 旅游行程最优化问题
