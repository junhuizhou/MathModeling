clear all;
close all;
clc;

pop = 40; % 种群数量
MaxIt = 2000; % 迭代次数
M = 2; % 目标函数个数
V = 30; % 维度(决策变量个数)
min_range = zeros(1, V); % 下界为0 变量定义域
max_range = ones(1, V); % 上界为1

% 取得初始化种群的同时调用evaluate_objective0函数计算目标值
% indiv返回 =[自变量值向量，目标值1，目标值2]
indiv = initialize_variables(pop, M, V, min_range, max_range);
% 对初始化种群进行非支配快速排序和拥挤度计算
% indiv返回 =[自变量值向量，目标值1，目标值2，排序等级，拥挤距离]
indiv = non_domination_sort_mod(indiv, M, V);

for i = 1:MaxIt
    pool = round(pop / 2); % 交配池大小
    tour = 2; % 竞标赛参赛选手个数

    % 竞标赛选出pool个新个体，优先选择排序等级高的个体
    % 如果排序等级一样，优选选择拥挤距离大的个体
    parent = tournament_selection(indiv, pool, tour);
    mu = 20; % 交叉和变异分布指数
    mum = 20;

    % 交叉变异产生N子代 该代码中使用模拟二进制交叉和多项式变异 采用实数编码
    offspring = genetic_operator(parent, M, V, mu, mum, min_range, max_range);
    [main_pop, ~] = size(indiv); % 父代种群的大小N
    [ofispring_pop, ~] = size(offspring); % 子代种群的大小N
    intermed(1:main_pop, :) = indiv;

    % 合并父代种群和子代种群
    intermed(main_pop + 1:main_pop + ofispring_pop, 1:M + V) = offspring;
    % 对新的种群进行快速非支配排序
    intermed = non_domination_sort_mod(intermed, M, V);
    % 选择合并种群中前N个优先的个体组成新种群
    indiv = replace_indiv(intermed, M, V, pop);

    if ~mod(i, 100)
        clc;
        fprintf('已经完成 % d of % d 次迭代l', i, MaxIt);
    end

end

fprintf('帕累托最优边界坐标');
fprintf('% d, % d\n', round(indiv(:, V + 1), 3), round(indiv(:, V + 2), 3));
scatter(indiv(:, V + 1), indiv(:, V +2), 'filled', 'b');
xlabel('f1');
ylabel('f2');
title('Pareto 最优边界图');
box on;

% 取得初始化种群的同时调用evaluate_objective()函数计算目标值
% 返回f=[自变量值向量，目标值1，目标值2]
function f = initialize_variables(N, M, V, min_range, max_range)
    min = min_range;
    max = max_range;
    K = V + M; % K矩阵元素个数
    % 对于交叉和变异，利用目标变量对决策变量进行选择
    for i = 1:N

        for j = 1:V
            f(i, j) = min(j) + (max(j) - min(j)) * rand; % 种群中所有变量在定义域内随机取值
        end

        % M目标函数数量 V变量个数，为便于计算目标函数值存在V+1到K的位置
        f(i, V +1:K) = evaluate_objective(f(i, :), M, V);

    end

end

% 对新的种群进行快速非支配排序
% x--种群矩阵[自变量值向量，目标值1,目标值2]；
% M--目标函数个数；V-变量个数
% z--返回包含排序和拥挤度的种群矩阵
function z = non_domination_sort_mod(x, M, V)

    [N, ~] = size(x); % N为矩阵x的行数，也是种群的数量
    front = 1;
    F(front).f = [];
    indiv = [];

    % 找出等级最高的非支配解集
    for i = 1:N
        indiv(i).n = 0; % n是个体i被支配的个体数量
        indiv(i).p = []; % p是被个体i支配的个体集合

        for j = 1:N
            dom_less = 0;
            dom_equal = 0;
            dom_more = 0;

            for k = 1:M % 判断个体i和个体j的支配关系

                if (x(i, V + k) < x(j, V + k))
                    dom_less = dom_less + 1;
                elseif (x(i, V + k) == x(j, V + k))
                    dom_equal = dom_equal + 1;
                else
                    dom_more = dom_more + 1;
                end

            end

            if dom_less == 0 && dom_equal ~= M % i受j支配，相应的n加1
                indiv(i).n = indiv(i).n + 1;
            elseif dom_more == 0 && dom_equal ~= M % i支配j把j加入i的支配合集中
                indiv(i).p = [indiv(i).p j];
            end

        end

        if indiv(i).n == 0 % 个体i非支配等级排序最高，属于当前最优解集
            x(i, M + V + 1) = 1;
            F(front).f = [F(front).f i]; % 等级为1的非支配解集
        end

    end

    % 给除了最高等级外的其他个体进行分级

    while ~isempty(F(front).f)
        Q = []; % 存放下一个front集合

        for i = 1:length(F(front).f) % 循环当前支配解集中的个体

            if ~isempty(indiv(F(front).f(i)).p) % 个体i有自己所支配的解集

                for j = 1:length(indiv(F(front).f(i)).p) % 循环个体i所支配解集中的个体
                    %这里表示个体j的被支配个数减1
                    indiv(indiv(F(front).f(i)).p(j)).n = indiv(indiv(F(front).f(i)).p(j)).n - 1;

                    if indiv(indiv(F(front).f(i)).p(j)).n == 0 % 非支配解集放入集合Q中
                        x(indiv(F(front).f(i)).p(j), M + V + 1) = front + 1;
                        Q = [Q indiv(F(front).f(i)).p(j)];
                    end

                end

            end

        end

        front = front + 1;
        F(front).f = Q;
    end

    % 对个体的代表排序等级的列向量进行升序排序
    [temp, index_of_fronts] = sort(x(:, M + V + 1));

    for i = 1:length(index_of_fronts)
        % 存放x矩阵按照排序等级升序排序后的矩阵
        sorted_based_on_front(i, :) = x(index_of_fronts(i), :);
    end

    current_index = 0;

    %  Crowding distance 计算个体的拥挤度
    for front = 1:(length(F) - 1) % 一共有length-1个排序等级
        distance = [];
        y = [];
        previous_index = current_index + 1;

        for i = 1:length(F(front).f)
            y(i, :) = sorted_based_on_front(current_index + i, :); % 等级为front的集合矩阵
        end

        current_index = current_index + i; % current_index = i
        sortedOnObj = []; % 存放基于拥挤距离排序的矩阵

        for i = 1:M
            [sortedOnObj, indexOfObj] = sort(y(:, V + i)); % 按照目标函数值排序
            sortedOnObj = [];

            for j = 1:length(indexOfObj)
                sortedOnObj(j, :) = y(indexOfObj(j), :); % 目标函数值排序后的x矩阵
            end

            f_max = sortedOnObj(length(indexOfObj), V + i);
            f_min = sortedOnObj(1, V +i);
            y(indexOfObj(length(indexOfObj)), M + V + 1 + i) = Inf;
            y(indexOfObj(1), M + V + 1 + i) = Inf;

            for j = 2:length(indexOfObj) - 1 % 除第一个和最后一个的个体
                next_obj = sortedOnObj(j + 1, V + i);
                previous_obj = sortedOnObj(j - 1, V + i);

                if (f_max - f_min == 0)
                    y(indexOfObj(j), M + V + 1 + i) = Inf;
                else
                    y(indexOfObj(j), M + V + 1 + i) = (next_obj - previous_obj) / (f_max - f_min);
                end

            end

        end

        distance(:, 1) = zeros(length(F(front).f), 1);

        for i = 1:M
            distance(:, 1) = distance(:, 1) + y(:, M + V + 1 + i);
        end

        y(:, M + V + 2) = distance;
        y = y(:, 1:M + V + 2);
        z(previous_index:current_index, :) = y;
    end

end

% 竞标赛选择法，每次随机选择两个个体，优先选择排序等级高的个体
% 如果排序等级一样，优选选择拥挤距离大的个体
function f = tournament_selection(indiv, pool_size, tour_size)
    [pop, variables] = size(indiv); % 获得种群数量和变量数量
    rank = variables - 1; % 个体向量中排序值所在位置
    distance = variables; % 个体向量中拥挤距离所在位置

    for i = 1:pool_size
        % 产生两个1~pop 之间的随机数
        candidate = [0, 0];

        while candidate(1) == candidate(2)
            candidate = ceil(pop * rand(1, 2));
        end

        candidate = sort(candidate); % 升序
        % 记录每个参赛者的排序等级和拥挤距离
        for j = 1:tour_size
            ranks(j) = indiv(candidate(j), rank);
            dist(j) = indiv(candidate(j), distance);
        end

        % 选择排序等级较小的参赛者
        min_candidate = find(ranks == min(ranks));
        % 如果两个参赛者的排序等级相等 则继续比较拥挤距离
        if length(min_candidate) ~= 1
            max_candidate = find(dist(min_candidate) == max(dist(min_candidate)));

            if length(max_candidate) ~= 1
                max_candidate = max_candidate(1);
            end

            f(i, :) = indiv(candidate(min_candidate(max_candidate)), :);
        else
            f(i, :) = indiv(candidate(min_candidate(1)), :);
        end

    end

end

% 精英选择策略选择N个新个体构成种群
function f = replace_indiv(intermediate_indiv, M, V, pop)
    [N, m] = size(intermediate_indiv);
    % 按照排序值大小升序排序
    [temp, index] = sort(intermediate_indiv(:, M + V + 1));
    sorted_indiv = intermediate_indiv(index, :);
    % 找到最大等级
    max_rank = max(intermediate_indiv(:, M + V + 1));
    preIndex = 0;

    for i = 1:max_rank
        curntIndex = max(find(sorted_indiv(:, M + V + 1) == i));

        if curntIndex >= pop % 个体数量超过N，选择拥挤距离大的纳入
            remaining = pop - preIndex;
            temp_pop = sorted_indiv(preIndex + 1:curntIndex, :);
            [temp_sort, temp_sort_index] = sort(temp_pop(:, M + V + 2), 'descend');

            for j = 1:remaining
                f(preIndex +j, :) = temp_pop(temp_sort_index(j), :);
            end

            return;
        elseif curntIndex < pop % 个体数量未超过N，全部纳入
            f(preIndex + 1:curntIndex, :) = sorted_indiv(preIndex + 1:curntIndex, :);
        end

        preIndex = curntIndex;
    end

end

% 计算目标函数值
function f = evaluate_objective(x, M, V)
    f = [];
    f(1) = x(1);
    g = 1;
    sum = 0;

    for i = 2:V
        sum = sum + x(i);
    end

    sum = 9 * (sum / (V - 1));
    g = g + sum;
    f(2) = g * (1 - sqrt(x(1) / g));
end

% //TODO 遗传算子
% 交叉变异产生N子代
% 使用模拟二进制交叉和多项式变异
% 采用实数编码
function result = genetic_operator(parent, M, V, mu, mum, min_range, max_range)
    result = parent(:, 1:M + V);
end
