%使用fmincon()求解非线性规划问题
%例  min  z=x1^2+x2^2+x3^2+8
%    s.t. x1^2-x2+x3^2>=0
%         x1+x2^2+x3^2<=20
%         x1+x2^2=2
%         x2+2*x3^2=3*x1
%         x1,x2,x3>=0

clear all;
clc;
%线性约束参数设置
x0 = rand(3,1);
A = [];
b = [];
Aeq = [];
beq = [];
lb = zeros(3,1);
ub = [];
%求解
options = optimset('largescale', 'off');
[x, y] = fmincon(@fun, x0, A, b, Aeq, beq, lb, ub, @nonlcon, options);
xmin = x
zmin = y

%目标函数
function z = fun(x)
    z = sum(x.^2)+8;
end
%非线性约束条件
function [c,ceq] = nonlcon(x)
    c = [-x(1)^2+x(2)-x(3)^2, x(1)+x(2)^2+x(3)^2-20];
    ceq = [x(1)+x(2)^2-2, x(2)+2*x(3)^2-3*x(1)];
end
