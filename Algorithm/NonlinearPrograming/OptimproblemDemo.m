% 使用optimproblem()求解非线性规划问题
%例  min  z=x1^2+x2^2+x3^2+8
%    s.t. x1^2-x2+x3^2>=0
%         x1+x2^2+x3^2<=20
%         x1+x2^2=2
%         x2+2*x3^2=3*x1
%         x1,x2,x3>=0

clear all;
clc;
%参数设置
x0 = rand(3,1);
A = [];
b = [];
Aeq = [];
beq = [];
lb = zeros(3,1);
ub = [];
%创建决策变量
x = optimvar('x', 3, 'LowerBound', 0);
%创建规划问题
prob = optimproblem('ObjectiveSense', 'min');
prob.Objective = sum(x.^2); %prob2struct转换不带常数项
%创建线性与非线性约束
cons1 = x(1)^2 - x(2) + x(3)^2 >= 0;
cons2 = x(1) + x(2)^2 + x(3)^2 <= 20;
cons3 = x(1) + x(2)^2 == 2;
cons4 = x(2) + 2*x(3)^2 - 3*x(1) == 0;
prob.Constraints.cons1 = cons1;
prob.Constraints.cons2 = cons2;
prob.Constraints.cons3 = cons3;
prob.Constraints.cons4 = cons4;
%问题转换结构
problem = prob2struct(prob);
problem.x0 = x0; %fmincon需要设置初始值
%求解
[sol,fval,exitflag,output] = fmincon(problem);
xmax = sol
zmax = fval+8 %补上常数项
