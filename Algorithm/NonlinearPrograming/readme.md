<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-18 19:24:13
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-18 21:42:16
 * @FilePath: \MathModeling\Algorithm\NonlinearPrograming\readme.md
 * @Description: file content
-->

# 非线性规划Matlab实现

## Matlab函数

* fmincon()
* optimproblem()
