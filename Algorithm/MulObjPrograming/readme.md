<!--
 * @Author: ZhouJunhui
 * @Date: 2022-10-19 16:33:16
 * @LastEditor: ZhouJunhui
 * @LastEditTime: 2022-10-19 20:55:48
 * @FilePath: \MathModeling\Algorithm\MulObjPrograming\readme.md
 * @Description: file content
-->

# 多目标规划Matlab实现

## 求解方法

* 合并目标函数变成单目标解法
    >* 线性加权
    >* 理想点法
    >* 优先级法
* 带精英策略的非支配排序遗传算法NSGA-II
    >* [NSGA-II](https://blog.csdn.net/paulfeng20171114/article/details/82454310)
    
## 使用函数

* gamultiobj()——ga即遗传，基于NSGA-II改进的算法
    >* x = gamultiobj(fun,nvars,A,b,Aeq,beq,lb,ub,nonlcon,options)
    >* x = gamultiobj(problem)
    >* 从选项看可以解非线性，但没有整数选型，可以对结果的决策变量取整再算，或者求解过程中取整
