% 使用gamultiobj()求解多目标规划问题
%例  min  z1=x1^4 - 10*x1^2 + x1*x2 + x2^4 - x1^2*x2^2
%    min  z2=x2^4 - x1^2*x2^2 + x1^4 + x1*x2
%    s.t. -5<=xi<=5

clear all;
clc;
%参数设置
nvars = 2;      %决策变量数量
lb = [-5, -5];  %变量下界
ub = [5, 5];    %变量上界
A = [];         %不等式约束左边
b = [];         %不等式约束右边
Aeq = [];       %等式约束左边
beq = [];       %等式约束右边
fun = @obj;     %目标函数
%求解器参数设置(help gamultiobj的输入项有说明option参数含义及推荐取值)
options = optimoptions('gamultiobj',...
                       'ParetoFraction',0.3,'PopulationSize',100,...
                       'Generations',200,'StallGenLimit',200,...
                       'TolFun',1e-100,'PlotFcns',@gaplotpareto);
%求解
[x,fval,exitflag,output] = gamultiobj(fun,nvars,A,b,Aeq,beq,lb,ub,options);
xmin = x
zmin = fval

%目标函数
function y = obj(x)
    y(1) = x(1)^4 - 10*x(1)^2 + x(1)*x(2) + x(2)^4 - (x(1)^2)*(x(2)^2);
    y(2) = x(2)^4 - (x(1)^2)*(x(2)^2) + x(1)^4 + x(1)*x(2);
end
