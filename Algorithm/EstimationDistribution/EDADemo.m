% 使用二进制编码的增量学习分布式估计算法PBIL求解01背包问题

clear all;
close all;
clc;

weight = [382745, 799601, 909247, 729069, 467902, 44328, 34610, 698150, 823460, ...
              903959, 853665, 551830, 610856, 670702, 488960, 951111, 323046, 446298, ...
              931161, 31385, 496951, 264724, 224916, 169684]; % 每个物件的体积
value = [825594, 1677009, 1676628, 1523970, 943972, 97426, 69666, 1296457, ...
             1679693, 1902996, 184992 1049289, 1252836, 1319836, 953277, 2067538, ...
             675367, 853655, 1826027, 65731, 901489, 577243, 466257, 369261]; % 每个物件的价值
weightMax = 6404180; % 背包最大容量

iterations = 1000; % 迭代最大次数
popSize = 200; % 种群规模
learningRate = 0.3; % 学习效率，太小收敛慢
maxSpec = 20; % 最大刷新次数
dominantNum = popSize * 0.1; % 优势群体个数
dim = size(weight, 2); % 维度

% 初始化种群
prob = 0.5 * ones(1, dim); % 初始化均以概率0.5产生变量
Best_Solution = zeros(iterations, dim + 1); % 保存每轮迭代的最优解
Species = zeros(popSize, dim); % 保存各个体受概率分布影响产生的解

% 循环迭代
for I = 1:iterations
    flag = 0;
    i = 1;
    % 针对每个个体
    while i <= popSize
        r = rand(1, dim); % 随机生成0~1之间规模为变量个数的随机值数组
        Species(i, :) = 1 .* (r < prob); % 按照概率模型创建样本
        weightSum = sum(Species(i, :) .* weight, 2); % 判断是否超出容量范围

        if flag >= maxSpec
            Species(i, :) = zeros(1, dim); % 多次仍难达到要求，随机生成一个个体
            flag = 0;
        elseif weightSum > weightMax % 超上限
            i = i - 1; % i累减
            flag = flag + 1; % flag累加
        else
            flag = 0;
        end

        i = i + 1;
    end

    Fitness = zeros(popSize, 1);

    for i = 1:popSize
        Fitness(i, 1) = sum(Species(i, :) .* value, 2); % 计算新种群适应度
    end

    [Fitness, index] = sort(Fitness); % 小到大排序，最后一个是本轮最佳值
    Best_Solution(I, 1) = I; % 第1列是序号，第2列是目标值，其余列是目标解
    Best_Solution(I, 2) = Fitness(index(popSize)); % 每轮迭代得到的最优解

    for i = 3:dim + 2 % 保存每轮迭代的最优解
        Best_Solution(I, i) = Species(index(popSize), i - 2);
    end

    % 选取优势种群
    domSpec = zeros(dominantNum, dim); % 创建选取的优势群体<popSize

    for i = 1:dominantNum % 取出dominantNum个好的个体作为最佳种群，参与概率分布计算
        domSpec(i, :) = Species(index(popSize - dominantNum + i), :);
    end

    % 更新概率模型
    Ones_Number = sum(domSpec);
    prob = (1 - learningRate) * prob + learningRate * Ones_Number / dominantNum; % PBIL计算公式
end

% 输出结果
disp(strcat('最优结果', ':', num2str(Best_Solution(iterations, 2))));
