% 使用连续粒子群算法求解Goldstein-Price函数

clear all;
close all;
clc;

% Step1.参数初始化
popsize = 10; % 种群规模
dimension = 2; % 变量维数
C1 = 2; % 学习因子
C2 = 1.8;
VMIN = -4; % 最小飞行速度
VMAX = 4; % 最大飞行速度
XMIN = -2.0; % 位置变化范围
XMAX = 2.0;
ItTimes = 1000; % 最大迭代次数
vmskmin = VMIN * ones(popsize, dimension); % 飞行速度下限
vmskmax = VMAX * ones(popsize, dimension); % 飞行速度上限
VRmin = ones(dimension, 1) * -10;
VRmax = ones(dimension, 1) * 10;
VR = [VRmin, VRmax];
posmaskmin = repmat(VR(1:dimension, 1)', popsize, 1); % 位置下限
posmaskmax = repmat(VR(1:dimension, 2)', popsize, 1); % 位置上限
posmaskmeth = 0;
vel = rand(popsize, dimension) * (VMAX - VMIN) + VMIN; % 初始化个体速度
Xpos = rand(popsize, dimension) * (XMAX - XMIN) + XMIN; % 初始化初始位置
Xpos = Xpos + vel;
minposmask_throwaway = Xpos <= posmaskmin; % 限位置（变量范围）
minposmask_keep = Xpos > posmaskmin;
maxposmask_throwaway = Xpos >= posmaskmax;
maxposmask_keep = Xpos < posmaskmax;
Xpos = (minposmask_throwaway .* posmaskmin) + (minposmask_keep .* Xpos);
Xpos = (maxposmask_throwaway .* posmaskmax) + (maxposmask_keep .* Xpos);

fitness = goldstein_priceFunc(Xpos, popsize); % 计算目标函数适值

LBestXpos = Xpos; % 初始化局部最佳位置
LBest = fitness; % 初始化局部最佳值
[GBest, pos] = min(fitness); % 全局最佳值GBest
GIXpos = Xpos(pos, :); % 初始化全局最佳位置

for i = 1:ItTimes
    % Step2.计算各粒子目标适值
    fitness = goldstein_priceFunc(Xpos, popsize); % 计算目标函数适值
    [bestVal, pos] = min(fitness);

    if GBest > bestVal
        GBest = bestVal; % 保存全局最优值
        GIXpos = Xpos(pos, :);
    end

    for j = 1:popsize

        if LBest(j) > fitness(j) % 保存局部最优值
            LBest(j) = fitness(j);
            LBestXpos(j, :) = Xpos(j, :);
        end

    end

    %  Step3.更新速度和位置
    iwt = 0.98 - 0.3 * i / ItTimes; % 惯性权重系数随迭代次数线性变化式
    kx = rand(popsize, 2); % 随机数矩阵
    et = rand(popsize, 2); % 随机数矩阵
    vel = iwt * vel + kx .* C1 .* (LBestXpos - Xpos) + et .* C2 .* (repmat(GIXpos, popsize, 1) - Xpos);
    vel = ((vel <= vmskmin) .* vmskmin) + ((vel > vmskmin) .* vel); % 限速
    vel = ((vel >= vmskmax) .* vmskmax) + ((vel < vmskmax) .* vel);
    Xpos = Xpos + vel; % 更新位置
    minposmask_throwaway = Xpos <= posmaskmin; % 限位置
    minposmask_keep = Xpos > posmaskmin;
    maxposmask_throwaway = Xpos >= posmaskmax;
    maxposmask_keep = Xpos < posmaskmax;
    Xpos = (minposmask_throwaway .* posmaskmin) + (minposmask_keep .* Xpos);
    Xpos = (maxposmask_throwaway .* posmaskmax) + (maxposmask_keep .* Xpos);
end

GBest
GIXpos

% 函数GOLDSTEIN-PRICE 是由西蒙弗雷泽大学Sonja Surjanovic等提供
% 该函数的详细参考信息，可访问 http:/www.sfu.ca/~ssurjano/
% INPUT:xx = [x1, x2]和种群数量pop
% OUTPUT:results, 函数值计算结果，最小值 = 3，x1 = 0.5, x2 - 0.25
function results = goldstein_priceFunc(xx, pop)
    results = zeros(1, pop);

    for i = 1:pop
        x1bar = 4 * xx(i, 1) - 2;
        x2bar = 4 * xx(i, 2) - 2;

        factla = (x1bar + x2bar + 1) ^ 2;
        factlb = 19 - 14 * x1bar + 3 * x1bar ^ 2 - 14 * x2bar + 6 * x1bar * x2bar + 3 * x2bar ^ 2;
        factl = 1 + factla * factlb;
        fact2a = (2 * x1bar - 3 * x2bar) ^ 2;
        fact2b = 18 - 32 * x1bar + 12 * x1bar ^ 2 + 48 * x2bar -36 * x1bar * x2bar +27 * x2bar ^ 2;
        fact2 = 30 + fact2a * fact2b;
        prod = factl * fact2;
        results(i) = prod;
    end

end
