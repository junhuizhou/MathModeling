% 使用离散粒子群算法求解0-1背包维妮塔

clear all;
close all;
clc;

Popsize = 10; % 粒子群规模
ItCycles = 100; % 迭代次数
dim = 10; % 决策变量的维度（物品个数）
c1 = 2; % 学习因子
c2 = 1.8;
w_max = 0.95; % 惯性权重最大值
w_min = 0.4; % 惯性权重最小值
v_max = 5; % 粒子飞行速度上下限
v_min = -5;
V = 300; % 背包总容量
capacity = [95 75 23 73 50 22 6 57 89 98]; % 物品的体积
price = [89 59 19 43 100 72 44 16 7 64]; % 物品的价值
penality = 2; % 惩罚系数
vel = v_min + rand(Popsize, dim) * (v_max -v_min); % 速度初始化
newPos = zeros(Popsize, size(capacity, 2)); % 位置初始化
indiBest = rand(Popsize, dim) > 0.5; % 个体最优位置初始化
indibestVal = zeros(Popsize, 1); % 个体最优适值初始化为0

for k = 1:Popsize % 计算每个解的适值
    indibestVal = funcKnapsack(indiBest, capacity, price, V, penality, Popsize);
end

globalBestSol = zeros(1, dim); % 全局最优解，初始化为全0二进制字符串
globalBestValue = 0; % 全局最优适值，初始化为0
vsig = zeros(Popsize, dim); % sigmoid速度转换向量

% 开始迭代
for gen = 1:ItCycles
    w = w_max - (w_max - w_min) * gen / ItCycles; % 动态惯性权值

    for k = 1:Popsize
        vel(k, :) = w * vel(k, :) + ...
            c1 * rand() * (indiBest(k, :) - newPos(k, :)) + ...
            c2 * rand() * (globalBestSol -newPos(k, :));

        for t = 1:dim % 限制粒子飞行速度不超过上下限

            if vel(k, dim) > v_max
                vel(k, dim) = v_max;
            end

            if vel(k, dim) < v_min
                vel(k, dim) = v_min;
            end

        end

        vsig(k, :) = 1 ./ (1 + exp(-vel(k, :))); % sigmoid函数式

        for t = 1:dim % 使用sigmoid函数转换速度映射

            if vsig(k, t) > rand()
                newPos(k, t) = 1;
            else
                newPos(k, t) = 0;
            end

        end

    end

    % 计算个体当前目标值
    new_fitness = funcKnapsack(newPos, capacity, price, V, penality, Popsize);

    for i = 1:Popsize % 保留个体当前最优解和最优目标值

        if new_fitness(i) > indibestVal(i)
            indiBest(i, :) = newPos(i, :);
            indibestVal(i, 1) = new_fitness(i);
        end

    end

    % 找最大值
    % 保留全局最优解和最优目标值
    [currentBest, index] = max(new_fitness);

    if currentBest > globalBestValue
        globalBestSol = indiBest(index, :);
        globalBestValue = currentBest;
    end

end

globalBestSol
globalBestValue

% 适应值函数
function results = funcKnapsack(pos, capacity, price, v, penality, popsize)
    results = zeros(1, popsize);
    cols = size(pos, 2);

    for i = 1:popsize
        value = 0;
        cap = 0;

        for j = 1:cols

            if pos(i, j) == 0
                continue;
            end

            cap = cap + pos(i, j) * capacity(j);

            if cap > v
                value = value - penality * pos(i, j) * price(j);
            else
                value = value + pos(i, j) * price(j);
            end

        end

        results(i) = value;
    end

end
