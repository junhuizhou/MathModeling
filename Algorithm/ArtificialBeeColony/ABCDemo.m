% 使用人工蜂群算法(Artificial Bee Colony, ABC)求解TSP问题

clear all;
close all;
clc;

% Stepl.初始化阶段
cityCoord = [8.54, 0.77, 17.02, 0.55, 18.47, 0.61, 10.36, 8.39, 4.85, 17.08, ...
                 3.38, 9.59, 7.01, 16.62, 10.84, 2.58, 5.02, 5.78, 17.33, 7.43;
             4.15, 2.52, 4.41, 12.03, 0.70, 11.51, 16.24, 4.47, 1.63, 13.80, ...
                 11.28, 4.66, 8.82, 12.65, 5.22, 9.67, 16.23, 6.34, 6.51, 0.55]; % 城市坐标
cityCoord = cityCoord';
cityNum = size(cityCoord, 1); % 城市数量
D = disMatrix(cityNum, cityCoord); % 计算距离矩阵
empBeeNum = 60; % 雇佣蜂数量
onlookBeeNum = 60; % 跟随蜂数量
colonySize = empBeeNum + onlookBeeNum; % 蜂群规模
MaxCycle = 1000; % 最大循环次数
Dim = cityNum; % 目标函数参数个数
Limit = empBeeNum * cityNum; % 控制参数：引领蜂淘汰上限
Colony = zeros(colonySize, cityNum); % 初始化种群=0
GlobalBest = 0; % 记录最佳目标值

for i = 1:colonySize
    Colony(i, :) = randperm(cityNum); % 随机生成初始种群
end

Employed = Colony(1:empBeeNum, :); % 取种群一半作为雇佣蜂，余下作为跟随蜂
solutionValue = calculatSolution(empBeeNum, D, cityNum, Employed); % 计算目标值
[GlobalMin, index] = min(solutionValue); % 最小目标函数值
bestSolution = Employed(index, :); % 初始化为最小目标解
Cycle = 1; % 循环计数置为1
reapetTime = zeros(1, empBeeNum); % 控制参数均置为0

while Cycle < MaxCycle
    % Step2.引领蜂邻域搜索阶段(employed phase)
    Employed2 = Employed; % 暂存原解

    for i = 1:empBeeNum % 每一只引领蜂都做一次邻域变换（可选方式多种）

        Param2Change = fix(rand * cityNum) + 1; % 随机选择一个维数值 1~cityNum
        neighbour = fix(rand * empBeeNum) + 1; % 只要是异于i的蜜源均可在1~empBeeNum之间
        % 必须选择一个异于i的蜜源
        while (neighbour == i)
            neighbour = fix(rand * empBeeNum) + 1; % 若相同必须选择另一个蜜源
        end

        tempOrig = Employed2(i, Param2Change); % 保留原解
        Employed2(i, Param2Change) = Employed(neighbour, Param2Change); % 改变变量值
        posi = find(Employed2(i, :) == Employed2(i, Param2Change)); % 找到值相同的位置

        if size(posi, 2) ~= 1 % 没有产生改变
            posi(Param2Change == posi) = []; % 删除变更的位置
            Employed2(i, posi) = tempOrig; % 恢复缺少的值
        end

    end

    % Step3.计算目标适值并使用贪心策略保留好的适值
    solutionValue2 = calculatSolution(empBeeNum, D, cityNum, Employed2); % 计算目标值

    for j = 1:empBeeNum % 贪心策略保留适值

        if solutionValue2(j) < solutionValue(j)
            reapetTime(j) = 0; % 目标如有改进，计数清零
            Employed(j, :) = Employed2(j, :); % 更新解
            solutionValue(j) = solutionValue2(j); % 更新目标值
        end

        reapetTime(j) = reapetTime(i) + 1; % 目标没有改进，计数+1
    end

    [currentBest, index] = min(solutionValue); % 目标函数最小值及其位置

    if currentBest < GlobalMin % 保留最佳结果
        GlobalMin = currentBest;
        bestSolution = Employed(index, :);
    end

    fiti = 1 ./ (1 + solutionValue);
    NormFit = fiti / sum(fiti); % 归一化
    Employed2 = Employed; % 回到舞蹈区把蜜源信息通过舞蹈传达给跟随蜂
    i = 1;
    t = 0;

    % Step4.跟随蜂跟随拽索阶段
    while (t < onlookBeeNum)

        if (rand < NormFit(i)) % 按照概率NormFit(i)选择是否跟随
            t = t + 1; % 若选择跟随，跟随峰做一次邻域变换（可选方式多种)
            Param2Change = fix(rand * cityNum) + 1;
            neighbour = fix(rand * onlookBeeNum) + 1;

            while (neighbour == i)
                neighbour = fix(rand * onlookBeeNum) + 1;
            end

            tempOrig = Employed2(i, Param2Change);
            Employed2(i, Param2Change) = Employed(neighbour, Param2Change);
            posi = find(Employed2(i, :) == Employed2(i, Param2Change));

            if size(posi, 2) ~= 1
                posi(Param2Change == posi) = []; % 删除变更的位置
                Employed2(i, posi) = tempOrig; % 恢复缺少的值
            end

        end

        i = i + 1;

        if (i == onlookBeeNum + 1) % 恢复到开始，继续选择跟随蜂
            i = 1;
        end

    end

    % Step5.计算目标适值并使用贪心策略保留好的适值
    solutionValue2 = calculatSolution(empBeeNum, D, cityNum, Employed2); % 计算目标值

    for j = 1:empBeeNum % 贪心策略保留新解

        if solutionValue2(j) < solutionValue(j)
            reapetTime(j) = 0;
            Employed(j, :) = Employed2(j, :);
            solutionValue(j) = solutionValue2(j);
        end

        reapetTime(j) = reapetTime(j) + 1;

    end

    [currentBest, index] = min(solutionValue2); % 目标函数最小值及其位置

    if currentBest < GlobalMin % 保留最佳结果
        GlobalMin = currentBest;
        bestSolution = Employed(index, :);
    end

    % Step6.淘汰低质量蜜蜂阶段
    for j = 1:empBeeNum

        if reapetTime(j) > Limit % 超过迭代次数上限
            reapetTime(j) = 0;
            Employed(j, :) = randperm(cityNum); % 随机生成一个新的初始解（转为侦查蜂）
        end

    end

    Cycle = Cycle + 1;

end

GlobalMin
bestSolution
DrawRoute(cityCoord, bestSolution);

% 计算距离矩阵
function D = disMatrix(n, Coord)
    D = zeros(n, n);

    for i = 1:n

        for j = 1:n

            if i ~= j
                D(i, j) = ((Coord(i, 1) - Coord(j, 1)) ^ 2 + (Coord(i, 2) - Coord(j, 2)) ^ 2) ^ 0.5;
            else
                D(i, j) = 0;
            end

            D(j, i) = D(i, j); % 对称矩阵
        end

    end

end

% 计算目标解
function solutionValue = calculatSolution(empBeeNum, D, cityNum, Employed)
    solutionValue = zeros(empBeeNum, 1);

    for i = 1:empBeeNum
        R = Employed(i, :);

        for j = 1:cityNum - 1
            solutionValue(i) = solutionValue(i) + D(R(j), R(j + 1));
        end

        solutionValue(i) = solutionValue(i) + D(R(1), R(cityNum));
    end

end

% 画出路径图
function DrawRoute(C, R)
    N = length(R);
    scatter(C(:, 1), C(:, 2)); % 坐标散点
    hold on;
    plot([C(R(1), 1), C(R(N), 1)], [C(R(1), 2), C(R(N), 2)], 'g'); % 画出连线
    hold on;

    for i = 2:N
        plot([C(R(i - 1), 1), C(R(i), 1)], [C(R(i - 1), 2), C(R(i), 2)], 'g'); % 画出连线
        hold on;
    end

    title('TSP优化效果图');
end
