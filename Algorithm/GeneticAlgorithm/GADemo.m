% 遗传算法求解Rosenbrock函数
% max f(x1,x2) = (1-x1)^2 + 100*(x2-x1^2)^2
% 变量(x1,x2)在区间[-2.048, 2.048]
% 最优解为f(-2.048, -2.048) = 3.9059e3
% 种群规模与变量个数成正比，一般取20-200
% 最大迭代数取100-1000
% 交叉概率取0.6-0.9
% 变异概率取0.001-0.1

clear all;
close all;
clc;

popsize = 200; % 种群规模
MaxIter = 1000; % 最大迭代数
P_C = 0.8; % 交叉概率
P_M = 0.01; % 变异概率
Length1 = 10; % 基因第一部分长度
Length2 = 10; % 基因第二部分长度
ChromLength = Length1 + Length2; % 基因总长度
IterNum = 0; % 记录迭代次数
Population = GenerateInitialPopulation(ChromLength, popsize); % 产生初始种群
Fitness = CalculateFitnessValue(popsize, Length1, Length2, Population); % 计算初始种群的目标函数值
[CurrentBest, BestIndex] = max(Fitness); % 种群中最大值及其位置
BestIndividual = Population(BestIndex, :); % 设为最佳个体
BestValue = CurrentBest; % 设为最佳目标函数值

% 开始循环迭代
while (IterNum < MaxIter)
    IterNum = IterNum + 1;
    [Fitness, x1, x2] = CalculateFitnessValue(popsize, Length1, Length2, Population); % 计算目标函数值
    Population = SeleRoulette(Population, Fitness, popsize); % 选择新一代种群
    Population = CrossoverOperator(popsize, Population, ChromLength, P_C); % 交叉
    Population = MutationOperator(popsize, ChromLength, P_M, Population); % 变异
    [Fitness, x1, x2] = CalculateFitnessValue(popsize, Length1, Length2, Population); % 计算新一代染色体
    [CurrentBest, BestIndex] = max(Fitness); % 最大值及其位置

    if CurrentBest >= BestValue % 保存最佳结果
        BestValue = CurrentBest;
        xl = x1;
        x2 = x2;
    end

end

% 显示最终结果
BestValue
x1
x2

% 随机产生初始种群函数
function pop = GenerateInitialPopulation(Chromlength, Popsize)
    pop = zeros(Popsize, Chromlength);

    for i = 1:Popsize

        for j = 1:Chromlength

            if (rand < 0.5)
                pop(i, j) = 0;
            else
                pop(i, j) = 1;
            end

        end

    end

end

% 计算日标函数值
function [Fitness, x1, x2] = CalculateFitnessValue(Popsize, Length1, Length2, pop)
    Fitness = zeros(1, Popsize);

    for i = 1:Popsize
        temp1 = DecodeChromosome(pop, 0, Length1, i);
        temp2 = DecodeChromosome(pop, Length1, Length2, i);
        x1 = 4.096 * temp1 / 1023 - 2.048;
        x2 = 4.096 * temp2 / 1023 - 2.048;
        Fitness(i) = 100 * (x1 ^ 2 - x2) ^ 2 + (1 - x1) ^ 2;
    end

end

% 轮盘赌函数：选择下一代种群
function newpop = SeleRoulette(pop, Fitness, Popsize)
    totalFitness = sum(Fitness); % 求适值之和
    pFitvalue = Fitness / totalFitness; % 单个个体被选择的概率
    mfitvalue = cumsum(pFitvalue); % 累计概率
    fitin = 1;
    newpop = zeros(size(pop)); % 用于存选出的新种群

    while fitin <= Popsize
        rd = rand; % 产生一个随机数

        for i = 1:Popsize

            if rd > mfitvalue(i) % 找到随机数所在区间
                continue;
            else
                newpop(fitin, :) = pop(i, :);
                fitin = fitin + 1;
                break;
            end

        end

    end

end

% 交叉函数：两点中间交叉
function newpop = CrossoverOperator(Popsize, pop, Chromlength, P_C)
    newpop1 = zeros(Popsize / 2, Chromlength); % 初始化新种群空间
    newpop2 = zeros(Popsize / 2, Chromlength);
    newpop = [];

    for i = 1:Popsize / 2
        point = 1 + randperm(Chromlength - 1, 2); % 随机两点交叉点

        while point(1) == point(2)
            point = randperm(Chromlength - 1, 2);
        end

        if point(1) > point(2) % 位置调整
            temp = point(1);
            point(1) = point(2);
            point(2) = temp;
        end

        temp1 = pop(i, :); % 取出两个原始个体，第i个与第Popsize/2+i个
        temp2 = pop(Popsize / 2 + i, :);
        p = rand;

        if (p < P_C)
            part1 = temp1(point(1):point(2)); % 第一个染色体中间部分
            part2 = temp2(point(1):point(2)); % 第二个染色体中间部分
            newpop1(i, :) = [temp1(1:point(1) - 1) part2 temp1(point(2) + 1:end)]; % 交换中间部分
            newpop2(i, :) = [temp2(1:point(1) - 1) part1 temp2(point(2) + 1:end)];
        else
            newpop1(i, :) = temp1;
            newpop2(i, :) = temp2;
        end

    end

    newpop = [newpop; newpop1; newpop2];
end

% 变异函数
function Poplation = MutationOperator(pop, chromlength, P_M, Poplation)

    for i = 1:pop
        p = rand; % 产生一个0~1之间的随机数
        point = randperm(chromlength, 1); % 产生一个1 ~chromlength 随机整数

        if (p < P_M) % 满足条件发生变异
            Poplation(i, point) = xor(Poplation(i, point), 1); % 与1发生异或操作
        end

    end

end

% 解码染色体
function deci = DecodeChromosome(POPULATION, point, length, j)
    deci = 0;

    for i = 1:length
        deci = deci + POPULATION(j, point + i) * 2 ^ (length -i);
    end

end
