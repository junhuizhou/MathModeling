A = xlsread("subway.xlsx");
S = size(A);
A(:,6) = A(:,5) - A(:,4);
xlswrite('subway2.xlsx',A);
for i = 1:S(1)-1
    if A(i+1,2) == A(i,2)
        A(i,7) = A(i,3);
        A(i,8) = A(i+1,3);
        A(i,9) = A(i+1,4) - A(i,5);
    else
        continue
    end
end
xlswrite('subway3.xlsx',A)
X = zeros(328);
Y = zeros(328);
Z = zeros(328);
for i = 1:328
    for j = 1:328
        Z(i,j) = inf;
    end
end
for k = 1:328
    Z(k,k) = 0;
end
for i = 1:S(1)
    if A(i,7) ~= 0
        X(A(i,7),A(i,8)) = X(A(i,7),A(i,8)) + A(i,9);
        Y(A(i,7),A(i,8)) = Y(A(i,7),A(i,8)) + 1;
    end
end
for i = 1:328
    for j = 1:328
        if X(i,j) ~= 0
            Z(i,j) = X(i,j) / Y(i,j);
        end
    end
end
xlswrite('subway4.xlsx',Z)
