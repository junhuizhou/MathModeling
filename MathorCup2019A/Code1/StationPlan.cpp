#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <fstream>
using namespace std;
const int infinity = 5000;
int bt[]={22,23,26,27,28,29,30,31,32,33,34,35,36};
vector<int> batong(bt,bt+13);

class station
{
public:
    vector<int> indx;
    vector<string> line;//所有线路
    int p; //前驱站点
    bool f; //是否被访问
    int d; //距离相隔站点数
    station(){};
    station(const vector<string> &_line, const int &_p, const bool &_f, const int &_d):line(_line),p(_p),f(_f),d(_d){};
};

string next_line(vector<string> a,vector<string> b);//是否与先前路径为同一条路线

void set_distance(vector<vector<int> > v,int source,int end_station, map<int,string> m, vector<station> sys)//贪心算法求最小路径
{
    int count[13]={0};
    int *distance = new int[v.size()]{}; //distance数组
    string current_line;
    station *vertex = new station[v.size()]; //vertex数组
    for(int i=0;i<v.size();i++) //初始化
    {
        vertex[i].f = false;
        vertex[i].p = source;
        vertex[i].d = infinity;//将所有站点的前驱节点都先设为起始点，方便第一次选择操作，距离设为无穷大
    }
    vertex[source].f=true; //把起点放入vertex集合
    vertex[source].d=0;
    for(int i=0;i<v.size();i++)
    {
        distance[i]= v[source][i]; //更新distance
    }
    int j = source; //j是distance最小的点的编号，source是为了避免错误的初值
    for(int z = 1;z<v.size();z++) //z是计数变量，把除起点外的点放入vertex集合
    {
        int min = infinity;
        for(int i=0;i<v.size();i++) // 找未放入集合的点中，distance最小的点
        {
            if(!vertex[i].f)
            {
                if(distance[i]<=min)
                {
                    min = distance[i];
                    j = i;
                }
            }
        }
        vertex[j].f = true;//标记为查找到
        vertex[j].d=min;
        current_line = next_line(sys[j].line,sys[vertex[j].p].line);
        //cout << m[j] << " "<<min<<" "<<current_line<<endl;
        if(min==infinity) // 如果min==infinity说明j点出度为0，可以跳过该步骤
        continue;
        for(int i=0;i<v[j].size();i++) //更新j点的邻接点的distance
        {
            if((!vertex[i].f)&&v[j][i]!=infinity)
            {
                int wei;
                if(current_line!=next_line(sys[i].line,sys[j].line))
                {
                    wei = v[j][i]+3;
                }
                else
                    wei = v[j][i];
                if(distance[i]>min+wei)
                {
                    distance[i] = min+wei;
                    vertex[i].p=j;
                }
            }
        }
    } //把所有点都放入了vertex集合
    string entertime,linenum;
    int nz = 0;
    current_line = next_line(sys[end_station].line,sys[vertex[end_station].p].line);
    cout << sys[end_station].indx[0]<< m[end_station];
    while(end_station!=source)
    {
        nz++;
        cout << " "<<sys[vertex[end_station].p].indx[0]<<m[vertex[end_station].p];
        end_station = vertex[end_station].p;
        if(end_station!=source&&current_line!=next_line(sys[end_station].line,sys[vertex[end_station].p].line))
        {
            current_line = next_line(sys[end_station].line,sys[vertex[end_station].p].line);//下一站非现在所在路线，则换乘
            cout <<"\nchange to "<<current_line<<" line\n";
        }
    }
    cout << "\ntotal "<<nz<<endl; //输出总需乘坐站数
    cout << endl<<endl;
}

string next_line(vector<string> a,vector<string> b) // 通过当前line与当前站点与下一站点的公共line是否相同判定是否换乘，并返回换乘线
{
    for(int i=0;i<a.size();i++)
    {
        for(int j=0;j<b.size();j++)
        {
            if(a[i]==b[j])
            {
                return a[i];
            }
        }
    }
    return "";
}

void create_adjacency_list(vector<vector<int>> &table, map<string,int> &m, map<int,string> &show_map, map<int,string> &cf, vector<station> &v)
{
    ifstream in;
    in.open("1.txt");
    string station_name;//站名
    string current_line="0"; //当前所在路线
    int index=0; //下标
    string r; //读入线路
    int last_index; //记录前一站点下标
    station temp;
    int num;
    in>>num>>station_name>>r;
    while(!in.eof())
    {
        cf[num]=station_name;
        if(m.find(station_name)==m.end()) // 当前m中没有该站点
        {
            m[station_name]=index;//创建新的站点
            show_map[index]=station_name;
            station temp;
            temp.line.push_back(r);//加入所在路线
            temp.indx.push_back(num);
            v.push_back(temp);//加入站点名
            vector<int> tv; //tv是temp vector用于建立邻接表，与前一站点发生联结，主要用于解决中转站
            if(current_line==r)//如果在同一条线上，则将当前站和前一站建立相连的边，体现在邻接表上
            {
                tv.push_back(last_index);
                table.push_back(tv);
                table[last_index].push_back(index);
            }
            else table.push_back(tv);
            last_index=index;
            index++;
        }
        else //如果不在一条线上，则捕鱼读取的前一站建立边
        {
            v[m[station_name]].line.push_back(r);
            if(current_line==r)
            {
                table[m[station_name]].push_back(last_index);
                table[last_index].push_back(m[station_name]);
                last_index = m[station_name];
            }
        }
        /*cout << m[station_name]<< station_name;
        for(int i=0;i<v[m[station_name]].line.size();i++)
        cout <<v[m[station_name]].line[i];
        cout <<endl;*/
        current_line = r;//调整当前路线
        in>>num>>station_name>>r;
    }
    in.close();
}

void create_adjacency_matrix(vector<vector<int> > &table,vector<vector<int> > &matrix)//生成的邻接矩阵做邻接表，方便贪心算法设计
{
    for(int i=0;i<table.size();i++)
    {
        for(int j=0;j<table.size();j++)
        {
            matrix[i].push_back(infinity);
        }
    }
    for(int i=0;i<table.size();i++)
    {
        for(int j=0;j<table[i].size();j++)
            matrix[i][table[i][j]]=1; //每条边的默认weigth是1
    }
}

int main()
{
    map<string,int> m; //由站名到邻接矩阵中的下标映射
    map<int,string> show_map; //由下标到站名的映射
    map<int,string> chongfu;
    //输入文件流
    //数据，保存格式为：站点命，路线号
    vector<station> v;//保存所有站点信息
    vector<vector<int>> table; //邻接表
    create_adjacency_list(table,m,show_map,chongfu,v); //建立邻接表
    vector<vector<int> > matrix; //邻接矩阵
    matrix.resize(table.size());//建立邻接矩阵
    create_adjacency_matrix(table,matrix);
    string begin,end;
    int b,e;
    cout << "Please input the beginning and the end\n";//输入出发点和终点
    while( cin >> begin >> end)
    {
        if(m.find(begin)==m.end()||m.find(end)==m.end())
        {
            cout << "The station isn't exist. The line haven't open.\n";
            continue;
        }
        set_distance(matrix,m[end],m[begin],show_map,v);
        cout << "Please input the beginning and the end\n";
    }
}
