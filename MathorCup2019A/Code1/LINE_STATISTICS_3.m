%% 地铁班次密集情况分线图
% 一号线1:1-23
[SS1,Y1] = STATISTICS3('line1_start_1.xlsx');
[SS2,Y2] = STATISTICS3('line1_start_23.xlsx');
xx1 = SS1(1):0.5:SS1(length(SS1));
yy1 = spline(SS1,Y1,xx1);
xx2 = SS1(1):0.5:SS2(length(SS2));
yy2 = spline(SS2,Y2,xx2);
scatter(SS1,Y1);hold on;scatter(SS2,Y2);plot(xx1,yy1,xx2,yy2);hold off;
title('Line1')
legend('Start from 1','Start from 23')
saveas(gcf,'line1SS','png')
% 二号线2：37-54
[SS1,Y1] = STATISTICS3('line2_start_37.xlsx');
[SS2,Y2] = STATISTICS3('line2_start_54.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line2')
legend('Start from 37','Start from 54')
saveas(gcf,'line2SS','png')
% 八通线3:24-36
[SS1,Y1] = STATISTICS3('line3_start_24.xlsx');
[SS2,Y2] = STATISTICS3('line3_start_36.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line3')
legend('Start from 24','Start from 36')
saveas(gcf,'line3SS','png')
% 四号线4:59-93
[SS1,Y1] = STATISTICS3('line4_start_59.xlsx');
[SS2,Y2] = STATISTICS3('line4_start_93.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line4')
legend('Start from 59','Start from 93')
saveas(gcf,'line4SS','png')
% 五号线5:94-116
[SS1,Y1] = STATISTICS3('line5_start_94.xlsx');
[SS2,Y2] = STATISTICS3('line5_start_116.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line5')
legend('Start from 94','Start from 116')
saveas(gcf,'line5SS','png')
% 六号线6：131-150,300-307
[SS1,Y1] = STATISTICS3('line6_start_131.xlsx');
[SS2,Y2] = STATISTICS3('line6_start_150.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line6A')
legend('Start from 131','Start from 150')
saveas(gcf,'line6ASS','png')
[SS1,Y1] = STATISTICS3('line6_start_300.xlsx');
[SS2,Y2] = STATISTICS3('line6_start_307.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line6B')
legend('Start from 24','Start from 36')
saveas(gcf,'line6BSS','png')
% 七号线7：308-328
[SS1,Y1] = STATISTICS3('line7_start_308.xlsx');
[SS2,Y2] = STATISTICS3('line7_start_328.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line7')
legend('Start from 308','Start from 328')
saveas(gcf,'line7SS','png')
% 八号线8：151-169
[SS1,Y1] = STATISTICS3('line8_start_151.xlsx');
[SS2,Y2] = STATISTICS3('line8_start_169.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line8')
legend('Start from 151','Start from 169')
saveas(gcf,'line8SS','png')
% 九号线9：170-181
[SS1,Y1] = STATISTICS3('line9_start_170.xlsx');
[SS2,Y2] = STATISTICS3('line9_start_181.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line9')
legend('Start from 170','Start from 181')
saveas(gcf,'line9SS','png')
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
[SS1,Y1] = STATISTICS3('line10_start_193.xlsx');
[SS2,Y2] = STATISTICS3('line10_start_237.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line10')
legend('Start from 193','Start from 237')
saveas(gcf,'line10SS','png')
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
[SS1,Y1] = STATISTICS3('line11_start_55.xlsx');
[SS2,Y2] = STATISTICS3('line11_start_58.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line11')
legend('Start from 55','Start from 58')
saveas(gcf,'line11SS','png')
% 亦庄线12：117-130
[SS1,Y1] = STATISTICS3('line12_start_117.xlsx');
[SS2,Y2] = STATISTICS3('line12_start_130.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line12')
legend('Start from 117','Start from 130')
saveas(gcf,'line12SS','png')
% 13号线13：238-253
[SS1,Y1] = STATISTICS3('line13_start_238.xlsx');
[SS2,Y2] = STATISTICS3('line13_start_253.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line13')
legend('Start from 238','Start from 253')
saveas(gcf,'line13SS','png')
% 14号线14：261-267,288-299
[SS1,Y1] = STATISTICS3('line14_start_261.xlsx');
[SS2,Y2] = STATISTICS3('line14_start_267.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line14A')
legend('Start from 24','Start from 36')
saveas(gcf,'line14ASS','png')
[SS1,Y1] = STATISTICS3('line14_start_288.xlsx');
[SS2,Y2] = STATISTICS3('line14_start_299.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line14B')
legend('Start from 24','Start from 36')
saveas(gcf,'line14BSS','png');
% 15号线15：268-287
[SS1,Y1] = STATISTICS3('line15_start_268.xlsx');
[SS2,Y2] = STATISTICS3('line15_start_287.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line15')
legend('Start from 268','Start from 287')
saveas(gcf,'line15SS','png');
% 昌平线18：254-260
[SS1,Y1] = STATISTICS3('line18_start_254.xlsx');
[SS2,Y2] = STATISTICS3('line18_start_260.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line18')
legend('Start from 254','Start from 260')
saveas(gcf,'line18SS','png');
% 房山线19：182-192
[SS1,Y1] = STATISTICS3('line19_start_182.xlsx');
[SS2,Y2] = STATISTICS3('line19_start_192.xlsx');
scatter(SS1,Y1);hold on;scatter(SS2,Y2);hold off;
title('Line19')
legend('Start from 182','Start from 192')
saveas(gcf,'line19SS','png');
