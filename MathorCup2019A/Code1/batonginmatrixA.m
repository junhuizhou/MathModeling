%真实进站时间为7:00-12:00，用0.2917-0.5表示，共5小时，统计每十分钟进站人数，共30行
%每行代表一个站点，如第一行为站点25，则第二行站点26，第12行站点36
A=zeros(12,300);
for i=26:37 %每分钟
    for j=1:length(stationbt) %真实车站值为j+25
        if i==stationbt(j)+1
            time=intimebt(j)*300./0.2083-420;
            time=ceil(time);
            A(i-25,time)=A(i-25,time)+1;
        end
    end
end
B=zeros(12,30);
for i=1:12 %间隔十分钟
    for j=1:10:300
        B(i,fix(j/10)+1)=B(i,fix(j/10)+1)+A(i,j)+...
            A(i,j+1)+A(i,j+2)+A(i,j+3)+A(i,j+4)+...
            A(i,j+5)+A(i,j+6)+A(i,j+7)+A(i,j+8)+A(i,j+9);
    end
end
xlswrite('matrixA.xlsx',B);
