%真实进站时间为4:48-12:00，用0.2-0.5表示，共432分钟，即共选取432个时间点，有432列
%每行代表一个站点，如第一行为站点25，则第二行站点26，第12行站点36
A=zeros(12,432);
for i=26:37
    for j=1:length(station) %真实车站值为j+25
        if i==station(j)+1
            time=intime(j)*432./0.3-288;
            time=floor(time);
            A(i-25,time)=A(i-25,time)+1;
        end
    end
end
