Bbefore9=zeros(1,328); %前半上午每个站点的进站人数
Bafter9=zeros(1,328); %后半上午每个站点的进站人数
BUSYb=zeros(1,328); %9点前的拥堵程度
BUSYa=zeros(1,328); %9点后的拥堵程度
for i=1:length(before9)
    Bbefore9(before9(i))=before9num(i);
end
for i=1:length(after9)
    Bafter9(after9(i))=after9num(i);
end
for m=1:328
    if Bbefore9(m)>9000
        BUSYb(m)=4;
    elseif Bbefore9(m)>6000
        BUSYb(m)=3;
    elseif Bbefore9(m)>2000
        BUSYb(m)=2;
    else
        BUSYb(m)=1;
    end
end
for m=1:328
    if Bafter9(m)>9000
        BUSYa(m)=4;
    elseif Bafter9(m)>6000
        BUSYa(m)=3;
    elseif Bafter9(m)>2000
        BUSYa(m)=2;
    else
        BUSYa(m)=1;
    end
end
BUSY=[BUSYb;BUSYa];
