clear all
% 发车时间间隔问题
A = xlsread("subway.xlsx");
%% 构造一堆数组
% 一号线1：1-23
line1_start_1 = [];
line1_reach_1 = [];
line1_start_23 = [];
line1_reach_23 = [];
% 二号线2：37-54
% 二号线是环线，以37号站位起点站，54号作为终点站
line2_start_37 = [];
line2_reach_37 = [];
line2_start_54 = [];
line2_reach_54 = [];
% 八通线3:24-36
line3_start_24 = [];
line3_reach_24 = [];
line3_start_36 = [];
line3_reach_36 = [];
% 四号线4:59-93
line4_start_59 = [];
line4_reach_59 = [];
line4_start_93 = [];
line4_reach_93 = [];
% 五号线5:94-116
line5_start_94 = [];
line5_reach_94 = [];
line5_start_116 = [];
line5_reach_116 = [];
% 六号线6：131-150,300-307
line6_start_131 = [];
line6_reach_131 = [];
line6_start_150 = [];
line6_reach_150 = [];
line6_start_300 = [];
line6_reach_300 = [];
line6_start_307 = [];
line6_reach_307 = [];
% 七号线7：308-328
line7_start_308 = [];
line7_reach_308 = [];
line7_start_328 = [];
line7_reach_328 = [];
% 八号线8：151-169
line8_start_151 = [];
line8_reach_151 = [];
line8_start_169 = [];
line8_reach_169 = [];
% 九号线9：170-181
line9_start_170 = [];
line9_reach_170 = [];
line9_start_181 = [];
line9_reach_181 = [];
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
line10_start_193 = [];
line10_reach_193 = [];
line10_start_237 = [];
line10_reach_237 = [];
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
line11_start_55 = [];
line11_reach_55 = [];
line11_start_58 = [];
line11_reach_58 = [];
% 亦庄线12：117-130
line12_start_117 = [];
line12_reach_117 = [];
line12_start_130 = [];
line12_reach_130 = [];
% :13号线13：238-253
line13_start_238 = [];
line13_reach_238 = [];
line13_start_253 = [];
line13_reach_253 = [];
% 14号线14：261-267,288-299
line14_start_261 = [];
line14_reach_261 = [];
line14_start_267 = [];
line14_reach_267 = [];
line14_start_288 = [];
line14_reach_288 = [];
line14_start_299 = [];
line14_reach_299 = [];
% 15号线15：268-287
line15_start_268 = [];
line15_reach_268 = [];
line15_start_287 = [];
line15_reach_287 = [];
% 昌平线18：254-260
line18_start_254 = [];
line18_reach_254 = [];
line18_start_260 = [];
line18_reach_260 = [];
% 房山线19：182-192
line19_start_182 = [];
line19_reach_182 = [];
line19_start_192 = [];
line19_reach_192 = [];
S = size(A);
%% 还要构造关于时间间隔的数组
% 一号线1:1-23
line1_gap_1 = [];
line1_gap_23 = [];
%% 开始循环
for i = 2:S(1)-1
%% 处理一号线
    % 一号线发车
    if A(i,3) == 23
        line1_reach_23 = horzcat(line1_reach_23,A(i,4));
        line1_start_23 = horzcat(line1_start_23,A(i,5));
    % 一号线到站
    elseif A(i,3) == 1
        line1_reach_1 = horzcat(line1_reach_23,A(i,4));
        line1_start_1 = horzcat(line1_start_23,A(i,5));
%% 处理二号线
    % 二号线发车
    elseif A(i,3) == 37
        line2_reach_37 = horzcat(line2_reach_37,A(i,4));
        line2_start_37 = horzcat(line2_start_37,A(i,5));
    % 二号线到站
    elseif A(i,3) == 54
        line2_reach_54 = horzcat(line2_reach_54,A(i,4));
        line2_start_54 = horzcat(line2_start_54,A(i,5));
%% 处理八通线
    % 八通线发车
    elseif A(i,3) == 24
        line3_reach_24 = horzcat(line3_reach_24,A(i,4));
        line3_start_24 = horzcat(line3_start_24,A(i,5));
    % 八通线到站
    elseif A(i,3) == 36
        line3_reach_36 = horzcat(line3_reach_36,A(i,4));
        line3_start_36 = horzcat(line3_start_36,A(i,5));
%% 处理四号线
    % 四号线发车
    elseif A(i,3) == 59
        line4_reach_59 = horzcat(line4_reach_59,A(i,4));
        line4_start_59 = horzcat(line4_start_59,A(i,5));
% 四号线到站
    elseif A(i,3) == 93
        line4_reach_93 = horzcat(line4_reach_93,A(i,4));
        line4_start_93 = horzcat(line4_start_93,A(i,5));
%% 处理五号线
    % 五号线发车
    elseif A(i,3) == 94
        line5_reach_94 = horzcat(line5_reach_94,A(i,4));
        line5_start_94 = horzcat(line5_start_94,A(i,5));
    % 五号线到站
    elseif A(i,3) == 116
        line5_reach_116 = horzcat(line5_reach_116,A(i,4));
        line5_start_116 = horzcat(line5_start_116,A(i,5));
%% 处理六号线
    % 六号线131
    elseif A(i,3) == 131
        line6_reach_131 = horzcat(line6_reach_131,A(i,4));
        line6_start_131 = horzcat(line6_start_131,A(i,5));
    % 六号线150
    elseif A(i,3) == 150
        line6_reach_150 = horzcat(line6_reach_150,A(i,4));
        line6_start_150 = horzcat(line6_start_150,A(i,5));
    % 六号线300
    elseif A(i,3) == 300
        line6_reach_300 = horzcat(line6_reach_300,A(i,4));
        line6_start_300 = horzcat(line6_start_300,A(i,5));
    % 六号线370
    elseif A(i,3) == 307
        line6_reach_307 = horzcat(line6_reach_307,A(i,4));
        line6_start_307 = horzcat(line6_start_307,A(i,5));
%% 处理七号线
    % 七号线308
    elseif A(i,3) == 308
        line7_reach_308 = horzcat(line7_reach_308,A(i,4));
        line7_start_308 = horzcat(line7_start_308,A(i,5));
    % 七号线328
    elseif A(i,3) == 328
        line7_reach_328 = horzcat(line7_reach_328,A(i,4));
        line7_start_328 = horzcat(line7_start_328,A(i,5));
%% 处理八号线
    % 八号线151
    elseif A(i,3) == 151
        line8_reach_151 = horzcat(line8_reach_151,A(i,4));
        line8_start_151 = horzcat(line8_start_151,A(i,5));
    % 八号线169
    elseif A(i,3) == 169
        line8_reach_169 = horzcat(line8_reach_169,A(i,4));
        line8_start_169 = horzcat(line8_start_169,A(i,5));
%% 处理九号线
    % 九号线170
    elseif A(i,3) == 170
        line9_reach_170 = horzcat(line9_reach_170,A(i,4));
        line9_start_170 = horzcat(line9_start_170,A(i,5));
    % 九号线181
    elseif A(i,3) == 181
        line9_reach_181 = horzcat(line9_reach_181,A(i,4));
        line9_start_181 = horzcat(line9_start_181,A(i,5));
%% 处理十号线
    % 十号线193
    elseif A(i,3) == 193
        line10_reach_193 = horzcat(line10_reach_193,A(i,4));
        line10_start_193 = horzcat(line10_start_193,A(i,5));
    % 十号线237
    elseif A(i,3) == 237
        line10_reach_237 = horzcat(line10_reach_237,A(i,4));
        line10_start_237 = horzcat(line10_start_237,A(i,5));
%% 处理机场线
    % 机场线55
    elseif A(i,3) == 55
        line11_reach_55 = horzcat(line11_reach_55,A(i,4));
        line11_start_55 = horzcat(line11_start_55,A(i,5));
    % 机场线58
    elseif A(i,3) == 58
        line11_reach_58 = horzcat(line11_reach_58,A(i,4));
        line11_start_58 = horzcat(line11_start_58,A(i,5));
%% 处理亦庄线
    % 亦庄线117
    elseif A(i,3) == 117
        line12_reach_117 = horzcat(line12_reach_117,A(i,4));
        line12_start_117 = horzcat(line12_start_117,A(i,5));
    % 亦庄线130
    elseif A(i,3) == 130
        line12_reach_130 = horzcat(line12_reach_130,A(i,4));
        line12_start_130 = horzcat(line12_start_130,A(i,5));
%% 处理十三号线
    % 十三号线238
    elseif A(i,3) == 238
        line13_start_238 = horzcat(line13_start_238,A(i,4));
        line13_reach_238 = horzcat(line13_reach_238,A(i,5));
    % 十三号线253
    elseif A(i,3) == 253
        line13_reach_253 = horzcat(line13_reach_253,A(i,4));
        line13_start_253 = horzcat(line13_start_253,A(i,5));
%% 处理十四号线
    % 十四号线261
    elseif A(i,3) == 261
        line14_reach_261 = horzcat(line14_reach_261,A(i,4));
        line14_start_261 = horzcat(line14_start_261,A(i,5));
    % 十四号线267
    elseif A(i,3) == 267
        line14_reach_267 = horzcat(line14_reach_267,A(i,4));
        line14_start_267 = horzcat(line14_start_267,A(i,5));
    % 十四号线288
    elseif A(i,3) == 288
        line14_reach_288 = horzcat(line14_reach_288,A(i,4));
        line14_start_288 = horzcat(line14_start_288,A(i,5));
    % 十四号线289
    elseif A(i,3) == 299
        line14_reach_299 = horzcat(line14_reach_299,A(i,4));
        line14_start_299 = horzcat(line14_start_299,A(i,5));
%% 处理十五号线
    % 十五号线268
    elseif A(i,3) == 268
        line15_reach_268 = horzcat(line15_reach_268,A(i,4));
        line15_start_268 = horzcat(line15_start_268,A(i,5));
    % 十五号线287
    elseif A(i,3) == 287
        line15_reach_287 = horzcat(line15_reach_287,A(i,4));
        line15_start_287 = horzcat(line15_start_287,A(i,5));
%% 处理昌平线
    % 昌平线254
    elseif A(i,3) == 254
        line18_reach_254 = horzcat(line18_reach_254,A(i,4));
        line18_start_254 = horzcat(line18_start_254,A(i,5));
    % 昌平线260
    elseif A(i,3) == 260
        line18_reach_260 = horzcat(line18_reach_260,A(i,4));
        line18_start_260 = horzcat(line18_start_260,A(i,5));
%% 处理房山线
    % 房山线182
    elseif A(i,3) == 182
        line19_reach_182 = horzcat(line19_reach_182,A(i,4));
        line19_start_182 = horzcat(line19_start_182,A(i,5));
    % 房山线192
    elseif A(i,3) == 192
        line19_reach_192 = horzcat(line19_reach_192,A(i,4));
        line19_start_192 = horzcat(line19_start_192,A(i,5));
    end
end
%% 由于时间没有按顺序，给时间排序以便于数据分析
% 一号线1：1-23
line1_start_1 = sort(line1_start_1);
line1_reach_1 = sort(line1_reach_1);
line1_start_23 = sort(line1_start_23);
line1_reach_23 = sort(line1_reach_23);
% 二号线2：37-54
line2_start_37 = sort(line2_start_37);
line2_reach_37 = sort(line2_reach_37);
line2_start_54 = sort(line2_start_54);
line2_reach_54 = sort(line2_reach_54);
% 八通线3：24-36
line3_start_24 = sort(line3_start_24);
line3_reach_24 = sort(line3_reach_24);
line3_start_36 = sort(line3_start_36);
line3_reach_36 = sort(line3_reach_36);
% 四号线4：59-93
line4_start_59 = sort(line4_start_59);
line4_reach_59 = sort(line4_reach_59);
line4_start_93 = sort(line4_start_93);
line4_reach_93 = sort(line4_reach_93);
% 五号线5：94-116
line5_start_94 = sort(line5_start_94);
line5_reach_94 = sort(line5_reach_94);
line5_start_116 = sort(line5_start_116);
line5_reach_116 = sort(line5_reach_116);
% 六号线6：131-150,300-307
line6_start_131 = sort(line6_start_131);
line6_reach_131 = sort(line6_reach_131);
line6_start_150 = sort(line6_start_150);
line6_reach_150 = sort(line6_reach_150);
line6_start_300 = sort(line6_start_300);
line6_reach_300 = sort(line6_reach_300);
line6_start_307 = sort(line6_start_307);
line6_reach_307 = sort(line6_reach_307);
% 七号线7：308-328
line7_start_308 = sort(line7_start_308);
line7_reach_308 = sort(line7_reach_308);
line7_start_328 = sort(line7_start_328);
line7_reach_328 = sort(line7_reach_328);
% 八号线8：151-169
line8_start_151 = sort(line8_start_151);
line8_reach_151 = sort(line8_reach_151);
line8_start_169 = sort(line8_start_169);
line8_reach_169 = sort(line8_reach_169);
% 九号线9：170-181
line9_start_170 = sort(line9_start_170);
line9_reach_170 = sort(line9_reach_170);
line9_start_181 = sort(line9_start_181);
line9_reach_181 = sort(line9_reach_181);
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
line10_start_193 = sort(line10_start_193);
line10_reach_193 = sort(line10_reach_193);
line10_start_237 = sort(line10_start_237);
line10_reach_237 = sort(line10_reach_237);
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
line11_start_55 = sort(line11_start_55);
line11_reach_55 = sort(line11_reach_55);
line11_start_58 = sort(line11_start_58);
line11_reach_58 = sort(line11_reach_58);
% 亦庄线12：117-130
line12_start_117 = sort(line12_start_117);
line12_reach_117 = sort(line12_reach_117);
line12_start_130 = sort(line12_start_130);
line12_reach_130 = sort(line12_reach_130);
% 13号线13：238-253
line13_start_238 = sort(line13_start_238);
line13_reach_238 = sort(line13_reach_238);
line13_start_253 = sort(line13_start_253);
line13_reach_253 = sort(line13_reach_253);
% 14号线14：261-267,288-299
line14_start_261 = sort(line14_start_261);
line14_reach_261 = sort(line14_reach_261);
line14_start_267 = sort(line14_start_267);
line14_reach_267 = sort(line14_reach_267);
line14_start_288 = sort(line14_start_288);
line14_reach_288 = sort(line14_reach_288);
line14_start_299 = sort(line14_start_299);
line14_reach_299 = sort(line14_reach_299);
% 15号线15：268-287
line6_start_300 = sort(line6_start_300);
line6_reach_300 = sort(line6_reach_300);
line6_start_307 = sort(line6_start_307);
line6_reach_307 = sort(line6_reach_307);
% 昌平线18：254-260
line18_start_254 = sort(line18_start_254);
line18_reach_254 = sort(line18_reach_254);
line18_start_260 = sort(line18_start_260);
line18_reach_260 = sort(line18_reach_260);
% 房山线19：182-192
line19_start_182 = sort(line19_start_182);
line19_reach_182 = sort(line19_reach_182);
line19_start_192 = sort(line19_start_192);
line19_reach_192 = sort(line19_reach_192);
%% 保存很多个一维数组
% 一号线1：1-23
xlswrite('line1_start_1.xlsx', line1_start_1);
xlswrite('line1_reach_1.xlsx', line1_reach_1);
xlswrite('line1_start_23.xlsx', line1_start_23);
xlswrite('line1_reach_23.xlsx', line1_reach_23);
% 二号线2：37-54
xlswrite('line2_start_37.xlsx', line2_start_37);
xlswrite('line2_reach_37.xlsx', line2_reach_37);
xlswrite('line2_start_54.xlsx', line2_start_54);
xlswrite('line2_reach_54.xlsx', line2_reach_54);
% 八通线3：24-36
xlswrite('line3_start_24.xlsx', line3_start_24);
xlswrite('line3_reach_24.xlsx', line3_reach_24);
xlswrite('line3_start_36.xlsx', line3_start_36);
xlswrite('line3_reach_36.xlsx', line3_reach_36);
% 四号线4：59-93
xlswrite('line4_start_59.xlsx', line4_start_59);
xlswrite('line4_reach_59.xlsx', line4_reach_59);
xlswrite('line4_start_93.xlsx', line4_start_93);
xlswrite('line4_reach_93.xlsx', line4_reach_93);
% 五号线5：94-116
xlswrite('line5_start_94.xlsx', line5_start_94);
xlswrite('line5_reach_94.xlsx', line5_reach_94);
xlswrite('line5_start_116.xlsx', line5_start_116);
xlswrite('line5_reach_116.xlsx', line5_reach_116);
% 六号线6：131-150,300-307
xlswrite('line6_start_131.xlsx', line6_start_131);
xlswrite('line6_reach_131.xlsx', line6_reach_131);
xlswrite('line6_start_150.xlsx', line6_start_150);
xlswrite('line6_reach_150.xlsx', line6_reach_150);
xlswrite('line6_start_300.xlsx', line6_start_300);
xlswrite('line6_reach_300.xlsx', line6_reach_300);
xlswrite('line6_start_307.xlsx', line6_start_307);
xlswrite('line6_reach_307.xlsx', line6_reach_307);
% 七号线7：308-328
xlswrite('line7_start_308.xlsx', line7_start_308);
xlswrite('line7_reach_308.xlsx', line7_reach_308);
xlswrite('line7_start_328.xlsx', line7_start_328);
xlswrite('line7_reach_328.xlsx', line7_reach_328);
% 八号线8：151-169
xlswrite('line8_start_151.xlsx', line8_start_151);
xlswrite('line8_reach_151.xlsx', line8_reach_151);
xlswrite('line8_start_169.xlsx', line8_start_169);
xlswrite('line8_reach_169.xlsx', line8_reach_169);
% 九号线9：170-181
xlswrite('line9_start_170.xlsx', line9_start_170);
xlswrite('line9_reach_170.xlsx', line9_reach_170);
xlswrite('line9_start_181.xlsx', line9_start_181);
xlswrite('line9_reach_181.xlsx', line9_reach_181);
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
xlswrite('line10_start_193.xlsx', line10_start_193);
xlswrite('line10_reach_193.xlsx', line10_reach_193);
xlswrite('line10_start_237.xlsx', line10_start_237);
xlswrite('line10_reach_237.xlsx', line10_reach_237);
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
xlswrite('line11_start_55.xlsx', line11_start_55);
xlswrite('line11_reach_55.xlsx', line11_reach_55);
xlswrite('line11_start_58.xlsx', line11_start_58);
xlswrite('line11_reach_58.xlsx', line11_reach_58);
% 亦庄线12：117-130
xlswrite('line12_start_117.xlsx', line12_start_117);
xlswrite('line12_reach_117.xlsx', line12_reach_117);
xlswrite('line12_start_130.xlsx', line12_start_130);
xlswrite('line12_reach_130.xlsx', line12_reach_130);
% 13号线13：238-253
xlswrite('line13_start_238.xlsx', line13_start_238);
xlswrite('line13_reach_238.xlsx', line13_reach_238);
xlswrite('line13_start_253.xlsx', line13_start_253);
xlswrite('line13_reach_253.xlsx', line13_reach_253);
% 14号线14：261-267,288-299
xlswrite('line14_start_261.xlsx', line14_start_261);
xlswrite('line14_reach_261.xlsx', line14_reach_261);
xlswrite('line14_start_267.xlsx', line14_start_267);
xlswrite('line14_reach_267.xlsx', line14_reach_267);
xlswrite('line14_start_288.xlsx', line14_start_288);
xlswrite('line14_reach_288.xlsx', line14_reach_288);
xlswrite('line14_start_299.xlsx', line14_start_299);
xlswrite('line14_reach_299.xlsx', line14_reach_299);
% 15号线15：268-287
xlswrite('line15_start_268.xlsx', line15_start_268);
xlswrite('line15_reach_268.xlsx', line15_reach_268);
xlswrite('line15_start_287.xlsx', line15_start_287);
xlswrite('line15_reach_287.xlsx', line15_reach_287);
% 昌平线18：254-260
xlswrite('line18_start_254.xlsx', line18_start_254);
xlswrite('line18_reach_254.xlsx', line18_reach_254);
xlswrite('line18_start_260.xlsx', line18_start_260);
xlswrite('line18_reach_260.xlsx', line18_reach_260);
% 房山线19：182-192
xlswrite('line19_start_182.xlsx', line19_start_182);
xlswrite('line19_reach_182.xlsx', line19_reach_182);
xlswrite('line19_start_192.xlsx', line19_start_192);
xlswrite('line19_reach_192.xlsx', line19_reach_192);
