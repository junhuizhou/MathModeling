%% 地铁班次密集情况分线图
X = [5:.5:12];
% 一号线1:1-23
X1 = STATISTICS('line1_start_1.xlsx');
X2 = STATISTICS('line1_start_23.xlsx');
P = plot(X,X1,X,X2);
title('Line1')
legend('Start from 1','Start from 23')
saveas(gcf,'line1','png')
% 二号线2：37-54
X1 = STATISTICS('line2_start_37.xlsx');
X2 = STATISTICS('line2_start_54.xlsx');
P = plot(X,X1,X,X2);
title('Line2')
legend('Start from 37','Start from 54')
saveas(gcf,'line2','png')
% 八通线3:24-36
X1 = STATISTICS('line3_start_24.xlsx');
X2 = STATISTICS('line3_start_36.xlsx');
P = plot(X,X1,X,X2);
title('Line3')
legend('Start from 24','Start from 36')
saveas(gcf,'line3','png')
% 四号线4:59-93
X1 = STATISTICS('line4_start_59.xlsx');
X2 = STATISTICS('line4_start_93.xlsx');
P = plot(X,X1,X,X2);
title('Line4')
legend('Start from 59','Start from 93')
saveas(gcf,'line4','png')
% 五号线5:94-116
X1 = STATISTICS('line5_start_94.xlsx');
X2 = STATISTICS('line5_start_116.xlsx');
P = plot(X,X1,X,X2);
title('Line5')
legend('Start from 94','Start from 116')
saveas(gcf,'line5','png')
% 六号线6：131-150,300-307
X1 = STATISTICS('line6_start_131.xlsx');
X2 = STATISTICS('line6_start_150.xlsx');
P = plot(X,X1,X,X2);
title('Line6A')
legend('Start from 131','Start from 150')
saveas(gcf,'line6A','png')
X1 = STATISTICS('line6_start_300.xlsx');
X2 = STATISTICS('line6_start_307.xlsx');
P = plot(X,X1,X,X2);
title('Line6B')
legend('Start from 24','Start from 36')
saveas(gcf,'line6B','png')
% 七号线7：308-328
X1 = STATISTICS('line7_start_308.xlsx');
X2 = STATISTICS('line7_start_328.xlsx');
P = plot(X,X1,X,X2);
title('Line7')
legend('Start from 308','Start from 328')
saveas(gcf,'line7','png')
% 八号线8：151-169
X1 = STATISTICS('line8_start_151.xlsx');
X2 = STATISTICS('line8_start_169.xlsx');
P = plot(X,X1,X,X2);
title('Line8')
legend('Start from 151','Start from 169')
saveas(gcf,'line8','png')
% 九号线9：170-181
X1 = STATISTICS('line9_start_170.xlsx');
X2 = STATISTICS('line9_start_181.xlsx');
P = plot(X,X1,X,X2);
title('Line9')
legend('Start from 170','Start from 181')
saveas(gcf,'line9','png')
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
X1 = STATISTICS('line10_start_193.xlsx');
X2 = STATISTICS('line10_start_237.xlsx');
P = plot(X,X1,X,X2);
title('Line10')
legend('Start from 193','Start from 237')
saveas(gcf,'line10','png')
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
X1 = STATISTICS('line11_start_55.xlsx');
X2 = STATISTICS('line11_start_58.xlsx');
P = plot(X,X1,X,X2);
title('Line11')
legend('Start from 55','Start from 58')
saveas(gcf,'line11','png')
% 亦庄线12：117-130
X1 = STATISTICS('line12_start_117.xlsx');
X2 = STATISTICS('line12_start_130.xlsx');
P = plot(X,X1,X,X2);
title('Line12')
legend('Start from 117','Start from 130')
saveas(gcf,'line12','png')
% 13号线13：238-253
X1 = STATISTICS('line13_start_238.xlsx');
X2 = STATISTICS('line13_start_253.xlsx');
P = plot(X,X1,X,X2);
title('Line13')
legend('Start from 238','Start from 253')
saveas(gcf,'line13','png')
% 14号线14：261-267,288-299
X1 = STATISTICS('line14_start_261.xlsx');
X2 = STATISTICS('line14_start_267.xlsx');
P = plot(X,X1,X,X2);
title('Line14A')
legend('Start from 24','Start from 36')
saveas(gcf,'line14A','png')
X1 = STATISTICS('line14_start_288.xlsx');
X2 = STATISTICS('line14_start_299.xlsx');
P = plot(X,X1,X,X2);
title('Line14B')
legend('Start from 24','Start from 36')
saveas(gcf,'line14B','png');
% 15号线15：268-287
X1 = STATISTICS('line15_start_268.xlsx');
X2 = STATISTICS('line15_start_287.xlsx');
P = plot(X,X1,X,X2);
title('Line15')
legend('Start from 268','Start from 287')
saveas(gcf,'line15','png');
% 昌平线18：254-260
X1 = STATISTICS('line18_start_254.xlsx');
X2 = STATISTICS('line18_start_260.xlsx');
P = plot(X,X1,X,X2);
title('Line18')
legend('Start from 254','Start from 260')
saveas(gcf,'line18','png');
% 房山线19：182-192
X1 = STATISTICS('line19_start_182.xlsx');
X2 = STATISTICS('line19_start_192.xlsx');
P = plot(X,X1,X,X2);
title('Line19')
legend('Start from 182','Start from 192')
saveas(gcf,'line19','png');
