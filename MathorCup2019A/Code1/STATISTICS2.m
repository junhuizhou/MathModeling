function [SS,Y] = STATISTICS2(line)
    S = xlsread(line);
    SS = S(1:length(S)-1);
    Y = zeros(1,length(S)-1);
    for i = 1:length(S)-1
        Y(i) = S(i+1) - S(i);
        if Y(i) < 0
            Y(i) = Y(i-1);
        end
    end
end
