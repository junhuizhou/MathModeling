function X = STATISTICS(line)
    S = xlsread(line);
    % 5点前 5点到5点半 半小时...11点半到12点
    X = zeros(1,15);
    for i = 1:length(S)
        if S(i) < 5/24
            X(1) = X(1) + 1;
        elseif S(i) < 5.5/24
            X(2) = X(2) + 1;
        elseif S(i) < 6/24
            X(3) = X(3) + 1;
        elseif S(i) < 6.5/24
            X(4) = X(4) + 1;
        elseif S(i) < 7/24
            X(5) = X(5) + 1;
        elseif S(i) < 7.5/24
            X(6) = X(6) + 1;
        elseif S(i) < 8/24
            X(7) = X(7) + 1;
        elseif S(i) < 8.5/24
            X(8) = X(8) + 1;
        elseif S(i) < 9/24
            X(9) = X(9) + 1;
        elseif S(i) < 9.5/24
            X(10) = X(10) + 1;
        elseif S(i) < 10/24
            X(11) = X(11) + 1;
        elseif S(i) < 10.5/24
            X(12) = X(12) + 1;
        elseif S(i) < 11/24
            X(13) = X(13) + 1;
        elseif S(i) < 11.5/24
            X(14) = X(14) + 1;
        elseif S(i) < 12/24
            X(15) = X(15) + 1;
        end
    end
end
