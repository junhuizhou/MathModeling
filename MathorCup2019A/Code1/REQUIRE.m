function [EARLY,LATE] = REQUIRE(line)
    S = xlsread(line);
    EARLY = S(1);
    LATE = S(length(S));
end
