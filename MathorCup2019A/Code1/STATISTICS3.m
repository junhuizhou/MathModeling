function [SS,Y] = STATISTICS3(line)
    S = xlsread(line);
    SS = S(1:length(S)-1);
    Y = zeros(1,length(S)-1);
    for i = 1:length(S)-1
        Y(i) = S(i+1) - S(i);
        if Y(i) < 2/1440
            SS(i) = -1;
            Y(i) = -1;
        end
    end
    SS(SS==-1)=[];
    Y(Y==-1)=[];
end
