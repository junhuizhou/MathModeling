%% 一些简单的数据处理
%% 寻找每条地铁线路的最早和最晚时间
P = [];
% 一号线1:1-23
[EARLY1,LATE1] = REQUIRE('line1_start_1.xlsx');
[EARLY2,LATE2] = REQUIRE('line1_reach_1.xlsx');
[EARLY3,LATE3] = REQUIRE('line1_start_23.xlsx');
[EARLY4,LATE4] = REQUIRE('line1_reach_23.xlsx');
P = vertcat(P,[1,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 二号线2：37-54
[EARLY1,LATE1] = REQUIRE('line2_start_37.xlsx');
[EARLY2,LATE2] = REQUIRE('line2_reach_37.xlsx');
[EARLY3,LATE3] = REQUIRE('line2_start_54.xlsx');
[EARLY4,LATE4] = REQUIRE('line2_reach_54.xlsx');
P = vertcat(P,[2,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 八通线3:24-36
[EARLY1,LATE1] = REQUIRE('line3_start_24.xlsx');
[EARLY2,LATE2] = REQUIRE('line3_reach_24.xlsx');
[EARLY3,LATE3] = REQUIRE('line3_start_36.xlsx');
[EARLY4,LATE4] = REQUIRE('line3_reach_36.xlsx');
P = vertcat(P,[3,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 四号线4:59-93
[EARLY1,LATE1] = REQUIRE('line4_start_59.xlsx');
[EARLY2,LATE2] = REQUIRE('line4_reach_59.xlsx');
[EARLY3,LATE3] = REQUIRE('line4_start_93.xlsx');
[EARLY4,LATE4] = REQUIRE('line4_reach_93.xlsx');
P = vertcat(P,[4,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 五号线5:94-116
[EARLY1,LATE1] = REQUIRE('line5_start_94.xlsx');
[EARLY2,LATE2] = REQUIRE('line5_reach_94.xlsx');
[EARLY3,LATE3] = REQUIRE('line5_start_116.xlsx');
[EARLY4,LATE4] = REQUIRE('line5_reach_116.xlsx');
P = vertcat(P,[5,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 六号线6：131-150,300-307
[EARLY1,LATE1] = REQUIRE('line6_start_131.xlsx');
[EARLY2,LATE2] = REQUIRE('line6_reach_131.xlsx');
[EARLY3,LATE3] = REQUIRE('line6_start_150.xlsx');
[EARLY4,LATE4] = REQUIRE('line6_reach_150.xlsx');
P = vertcat(P,[6,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
[EARLY1,LATE1] = REQUIRE('line6_start_300.xlsx');
[EARLY2,LATE2] = REQUIRE('line6_reach_300.xlsx');
[EARLY3,LATE3] = REQUIRE('line6_start_307.xlsx');
[EARLY4,LATE4] = REQUIRE('line6_reach_307.xlsx');
P = vertcat(P,[6,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 七号线7：308-328
[EARLY1,LATE1] = REQUIRE('line7_start_308.xlsx');
[EARLY2,LATE2] = REQUIRE('line7_reach_308.xlsx');
[EARLY3,LATE3] = REQUIRE('line7_start_328.xlsx');
[EARLY4,LATE4] = REQUIRE('line7_reach_328.xlsx');
P = vertcat(P,[7,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 八号线8：151-169
[EARLY1,LATE1] = REQUIRE('line8_start_151.xlsx');
[EARLY2,LATE2] = REQUIRE('line8_reach_151.xlsx');
[EARLY3,LATE3] = REQUIRE('line8_start_169.xlsx');
[EARLY4,LATE4] = REQUIRE('line8_reach_169.xlsx');
P = vertcat(P,[8,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 九号线9：170-181
[EARLY1,LATE1] = REQUIRE('line9_start_170.xlsx');
[EARLY2,LATE2] = REQUIRE('line9_reach_170.xlsx');
[EARLY3,LATE3] = REQUIRE('line9_start_181.xlsx');
[EARLY4,LATE4] = REQUIRE('line9_reach_181.xlsx');
P = vertcat(P,[9,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 十号线10：193-237
% 十号线为环线，以193号为起点站，以237号为终点站
[EARLY1,LATE1] = REQUIRE('line10_start_193.xlsx');
[EARLY2,LATE2] = REQUIRE('line10_reach_193.xlsx');
[EARLY3,LATE3] = REQUIRE('line10_start_237.xlsx');
[EARLY4,LATE4] = REQUIRE('line10_reach_237.xlsx');
P = vertcat(P,[10,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 机场线11：55-58
% 机场线为环线，以55号为起点站，以58号为终点站
[EARLY1,LATE1] = REQUIRE('line11_start_55.xlsx');
[EARLY2,LATE2] = REQUIRE('line11_reach_55.xlsx');
[EARLY3,LATE3] = REQUIRE('line11_start_58.xlsx');
[EARLY4,LATE4] = REQUIRE('line11_reach_58.xlsx');
P = vertcat(P,[11,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 亦庄线12：117-130
[EARLY1,LATE1] = REQUIRE('line12_start_117.xlsx');
[EARLY2,LATE2] = REQUIRE('line12_reach_117.xlsx');
[EARLY3,LATE3] = REQUIRE('line12_start_130.xlsx');
[EARLY4,LATE4] = REQUIRE('line12_reach_130.xlsx');
P = vertcat(P,[12,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 13号线13：238-253
[EARLY1,LATE1] = REQUIRE('line13_start_238.xlsx');
[EARLY2,LATE2] = REQUIRE('line13_reach_238.xlsx');
[EARLY3,LATE3] = REQUIRE('line13_start_253.xlsx');
[EARLY4,LATE4] = REQUIRE('line13_reach_253.xlsx');
P = vertcat(P,[13,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 14号线14：261-267,288-299
[EARLY1,LATE1] = REQUIRE('line14_start_261.xlsx');
[EARLY2,LATE2] = REQUIRE('line14_reach_261.xlsx');
[EARLY3,LATE3] = REQUIRE('line14_start_267.xlsx');
[EARLY4,LATE4] = REQUIRE('line14_reach_267.xlsx');
P = vertcat(P,[14,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
[EARLY1,LATE1] = REQUIRE('line14_start_288.xlsx');
[EARLY2,LATE2] = REQUIRE('line14_reach_288.xlsx');
[EARLY3,LATE3] = REQUIRE('line14_start_299.xlsx');
[EARLY4,LATE4] = REQUIRE('line14_reach_299.xlsx');
P = vertcat(P,[14,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 15号线15：268-287
[EARLY1,LATE1] = REQUIRE('line15_start_268.xlsx');
[EARLY2,LATE2] = REQUIRE('line15_reach_268.xlsx');
[EARLY3,LATE3] = REQUIRE('line15_start_287.xlsx');
[EARLY4,LATE4] = REQUIRE('line15_reach_287.xlsx');
P = vertcat(P,[15,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 昌平线18：254-260
[EARLY1,LATE1] = REQUIRE('line18_start_254.xlsx');
[EARLY2,LATE2] = REQUIRE('line18_reach_254.xlsx');
[EARLY3,LATE3] = REQUIRE('line18_start_260.xlsx');
[EARLY4,LATE4] = REQUIRE('line18_reach_260.xlsx');
P = vertcat(P,[18,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
% 房山线19：182-192
[EARLY1,LATE1] = REQUIRE('line19_start_182.xlsx');
[EARLY2,LATE2] = REQUIRE('line19_reach_182.xlsx');
[EARLY3,LATE3] = REQUIRE('line19_start_192.xlsx');
[EARLY4,LATE4] = REQUIRE('line19_reach_192.xlsx');
P = vertcat(P,[19,EARLY1,LATE1,EARLY2,LATE2,EARLY3,LATE3,EARLY4,LATE4]);
xlswrite('INFORMATION.xlsx',P)
